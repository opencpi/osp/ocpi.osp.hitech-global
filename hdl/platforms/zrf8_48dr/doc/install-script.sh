#!/bin/bash
set -e

export OCPI_XILINX_VERSION=2021.1
unset LD_LIBRARY_PATH

echo ">>>>>>>>>> Installing OpenCPI for your host" 
git clone https://gitlab.com/opencpi/opencpi.git opencpi

# the commit id 060f8d4717b5f4eb8828e183e4e5276fbc461f05
# branch as of 01/31/25
cd opencpi
git checkout 060f8d4717b5f4eb8828e183e4e5276fbc461f05
./scripts/install-opencpi.sh --minimal
source ./cdk/opencpi-setup.sh -r 

echo ">>>>>>>>>> Obtaining Hi-Tech Global OSP"
# below line is a copy, and not a symlink, so that that "cp relative directories ../../assets" lines below work as intended
cp -rp ../ocpi.osp.hitech-global projects/osps/ocpi.osp.hitech-global
cd projects/osps/ocpi.osp.hitech-global/
ocpidev register project 
./prerequisites/rfdc/install-rfdc.sh

echo ">>>>>>>>>> Moving OSP Artifacts"
cp ./hdl/platforms/zrf8_48dr/doc/gsg_artifacts/build.tcl ../../assets/hdl/assemblies/testbias/.
cp ./hdl/platforms/zrf8_48dr/doc/gsg_artifacts/testbias.Makefile.patch ../../assets/hdl/assemblies/testbias/.
cd ../../assets/hdl/assemblies/testbias/
git apply testbias.Makefile.patch 
cd ../../../../../ 

echo ">>>>>>>>>> Building core HDL pritivies for zynq_ultra" 
cd projects/core/hdl/primitives
ocpidev build --hdl-target zynq_ultra
cd ../../../../

echo ">>>>>>>>>> Cloning ocpi.comp.sdr library project, running initial builds" 
git clone https://gitlab.com/opencpi/comp/ocpi.comp.sdr.git projects/ocpi.comp.sdr
cd projects/ocpi.comp.sdr/
# the commit id 987ac9ed41f0b96789b279ce5f7a90a2570f7b9e points to the develop
# branch as of 01/15/25
git checkout 987ac9ed41f0b96789b279ce5f7a90a2570f7b9e
ocpidev register project 
cd hdl/primitives 
# TODO investigate order of hdl primitive builds
ocpidev build -d sdr_util --hdl-platform zrf8_48dr
ocpidev build --hdl-platform zrf8_48dr
cd ../../
ocpidev build
cd ../../

echo ">>>>>>>>>> Building HTG primitivies"
cd projects/osps/ocpi.osp.hitech-global/hdl/primitives
ocpidev build --hdl-target zynq_ultra
cd ../../../../../

echo ">>>>>>>>>> Building assets primitivies"
cd projects/assets/hdl/primitives
ocpidev build --hdl-target zynq_ultra
cd ../../../../

echo ">>>>>>>>>> Build HDL Device" 
cd projects/assets/hdl/devices/pps_fs_div_4_ts.hdl
ocpidev build --hdl-target zynq_ultra 
cd ../../../../../

ocpiadmin install platform zrf8_48dr --minimal
 
echo ">>>>>>>>>> Install xilinx21_1_aarch64 RCC platform" 
ocpiadmin install platform xilinx21_1_aarch64 --minimal

echo ">>>>>>>>>> Dev Environment Install Complete" 
