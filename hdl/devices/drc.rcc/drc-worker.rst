.. drc.rcc RCC worker

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.

..

.. _drc.rcc-RCC-worker:


``drc.rcc`` RCC Worker
======================

Digital Radio Controller (DRC) for the Xilinx RF Data Converter on the HiTech Global ZRF8 (only the 48DR variant is currently supported).

Detail
------

The worker's slave relationships are shown below.

.. figure:: drc_slaves.svg

The top-down architecture of the RF Data Converter Architecture using the
Digital Radio Controller (DRC) control interface is shown below.
The worker is designed around the Xilinx RF Data Converter FPGA IP (v2.5) and
the rfdc software library which is added as an OpenCPI prerequisite.
This prerequisite exists in the rfdc branch of a fork of opencpi.git at the
time of writing this document. The OpenCPI support stack for the RF Data
Converter adapts both the RF Data Converter IP and the rfdc software library
to use the OpenCPI HDL worker control plane for register access.

.. figure:: drc_rfdc_architecture.svg

A high-level, non-comprehensive C++ class diagram of the worker is shown below.

.. figure:: htg_drc_uml_diagram.png

Tested Platforms
----------------
* xilinx21_1_aarch64 (RCC platform) and zrf8_48dr (platform of the HDL slaves)

.. ocpi_documentation_worker::

rfdc and libmetal Software Library Dependencies
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
The rfdc software library is added as an OpenCPI prerequisite to support
runtime configuration and access of the RF Data Converter. The libmetal
software library that is built-in ("in-tree") to the rfdc repo is used. The
OpenCPI prerequisite build scripts patch libmetal's "linux" system to forward
libmetal io accesses to callback methods, e.g., get_ushort_prop(), that are later
defined in this worker's DeviceCallback class which has access to the
control plane of HDL slave workers. These callback methods are declared
in the opencpi repo's rfdc prerequisite .patch file. The libmetal library has a
build dependency on sysfs, which is the usual libmetal method of retrieving
buildtime-embedded system information regarding the RF Data Converter. Because
OpenCPI has no such paradigm, i.e., OpenCPI FPGA builds will not produce any
device tree output that is readable by sysfs at OpenCPI runtime, this worker's
use of the rfdc/libmetal library bypasses sysfs entirely. By not calling the
XRFdc_Lookup_Config() API call, as is normally seen in rfdc examples, sysfs is
effectively bypassed. These are all critical design details that allow
the Xilinx RF Data Converter software and FPGA to interact in the standard
OpenCPI fashion.

RF Data Converter Sample Clocking
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
The HiTech Global ZRF8 uses an LMX2594 device to generate the sampling clock for
the RF data converter. The C++ classes herein use lmx2594_proxy.rcc slave to
effectively initialize the LMX2594 to have output rates of 3600 MHz for both its
outputs, which drive the various ADC/DAC clock inputs of the RF Data Converter FPGA IP.
Optionally, users can enter the filename of their own Analog Devices
TICS-Pro-generated file via the tics_pro_filename property of this DRC worker,
which overrides the default LMX2594 3600 MHz.configuration, but this is not
recommended as it could invalidate clock rate assumptions that are hardcoded
within the ZRF8DRC-related C++ classes.
Note that the RF Data Converter IP instances
decimators/interpolators for all interfaces with a factor of 40, so the
effective sampling rate exposed to the OpenCPI data plane is 90 Msps.
Care is taken to ensure LMX2594 initialization occurs according to the RF Data
Converter requirements, especially with respect to RF Data Converter FPGA
IP AXI-Lite and AXI-Stream reset ordering. A good indication that the RF Data
Converter IP has a good clock configuration and fully completed its
Power-On Reset (POR) sequence is the worker runtime logs indicating a POR
state (denoted as st in the log below) of 15 for all relevant tiles.
The log is in the format "[DEBUG] rfdc <adc/dac> tl<tile#> <config> <value>"::

  [INFO] rfdc XRFdc_CfgInitialize
  [INFO] rfdc XRFdc_GetIPStatus
  [DEBUG] rfdc d tl2 en 1
  [DEBUG] rfdc d tl2 st 15
  [DEBUG] rfdc d tl2 pu 1
  [DEBUG] rfdc d tl2 pl 0
  [DEBUG] rfdc d tl3 en 1
  [DEBUG] rfdc d tl3 st 15
  [DEBUG] rfdc d tl3 pu 1
  [DEBUG] rfdc d tl3 pl 0
  [DEBUG] rfdc a tl0 en 1
  [DEBUG] rfdc a tl0 st 15
  [DEBUG] rfdc a tl0 pu 1
  [DEBUG] rfdc a tl0 pl 0
  [DEBUG] rfdc a tl1 en 1
  [DEBUG] rfdc a tl1 st 15
  [DEBUG] rfdc a tl1 pu 1
  [DEBUG] rfdc a tl1 pl 0
  [DEBUG] rfdc a tl2 en 1
  [DEBUG] rfdc a tl2 st 15
  [DEBUG] rfdc a tl2 pu 1
  [DEBUG] rfdc a tl2 pl 0
  [DEBUG] rfdc a tl3 en 1
  [DEBUG] rfdc a tl3 st 15
  [DEBUG] rfdc a tl3 pu 1
  [DEBUG] rfdc a tl3 pl 0

RF Port Configuration
---------------------

The follwing configurations ranges are supported by this DRC worker:

   - rf_port_name J18
      - rx                 : true
      - tuning_freq_MHz    : [180,405] with step size less than 0.000001 MHz
      - bandwidth_3dB_MHz  : 100
      - sampling_rate_Msps : 90
      - gain_dB            : [-27,0] with step size of 1 dB
      - app_port_num       : 1
   - rf_port_name J3
      - rx                 : false
      - tuning_freq_MHz    : [180,855] with step size less than 0.000001 MHz
      - bandwidth_3dB_MHz  : 150
      - sampling_rate_Msps : 90
      - gain_dB            : 0
      - app_port_num       : 1
   - rf_port_name J20
      - rx                 : true
      - tuning_freq_MHz    : [180,405] with step size less than 0.000001 MHz
      - bandwidth_3dB_MHz  : 100
      - sampling_rate_Msps : 90
      - gain_dB            : [-27,0] with step size of 1 dB
      - app_port_num       : 0
   - rf_port_name J13
      - rx                 : false
      - tuning_freq_MHz    : [180,855] with step size less than 0.000001 MHz
      - bandwidth_3dB_MHz  : 150
      - sampling_rate_Msps : 90
      - gain_dB            : 0
      - app_port_num       : 0

