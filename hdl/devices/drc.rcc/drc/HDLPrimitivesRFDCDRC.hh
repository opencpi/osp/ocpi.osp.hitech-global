/*
 * This file is protected by Copyright. Please refer to the COPYRIGHT file
 * distributed with this source distribution.
 *
 * This file is part of OpenCPI <http://www.opencpi.org>
 *
 * OpenCPI is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _HDL_PRIMITIVES_RFDCDRC_HH
#define _HDL_PRIMITIVES_RFDCDRC_HH

#include "RFDCDRC.hh"

/// @brief DEPENDENCIES: RFDCDRC class (RFDC SUPPORT)
class HDLPrimitivesRFDCDRC : public RFDCZU4XDRDRC {
  public:
  HDLPrimitivesRFDCDRC(RFDCCallBack &rfdc);
  uint8_t get_rfsoc_mixer_freq_constraint_mode();
  void set_rfsoc_mixer_freq_constraint_mode(uint8_t val);
  void initialize(
      const std::vector<std::string>& rf_ports_rx,
      const std::vector<std::string>& rf_ports_tx,
      const std::string& rf_port_name_rfdc_adc0,
      const std::string& rf_port_name_rfdc_adc2,
      const std::string& rf_port_name_rfdc_dac32,
      const std::string& rf_port_name_rfdc_dac30);
  virtual void set_start(uint16_t val);
  virtual void set_stop(uint16_t val);
  protected:
  /// @brief 0 for default (constrained) mode, 1 for experimental unconstrained
  uint8_t m_rfsoc_mixer_freq_constraint_mode;
  std::string m_rf_port_name_rfdc_adc0;
  std::string m_rf_port_name_rfdc_adc2;
  std::string m_rf_port_name_rfdc_dac32;
  std::string m_rf_port_name_rfdc_dac30;
  OperatingPlan get_operating_plan_for_rx(
      const ConfigurationChannel& chan, const std::string& rf_port_name,
      double val);
  OperatingPlan get_operating_plan_for_tuning_freq_MHz(
      const ConfigurationChannel& chan, const std::string& rf_port_name,
      double val, double tolerance);
  OperatingPlan get_operating_plan_for_samples_are_complex(
      const ConfigurationChannel& chan, const std::string& rf_port_name,
      double val);
  OperatingPlan get_operating_plan_for_gain_mode(
      const ConfigurationChannel& chan, const std::string& rf_port_name,
      double val);
  int get_expected_ip_version_major();
  int get_expected_ip_version_minor();
  bool get_any_dac_tile_is_operating();
  void init_xrfdc_config();
  void init_rf_port_infos();
}; // class HDLPrimitivesRFDCDRC

#include "HDLPrimitivesRFDCDRC.cc"

#endif // _HDL_PRIMITIVES_RFDCDRC_HH
