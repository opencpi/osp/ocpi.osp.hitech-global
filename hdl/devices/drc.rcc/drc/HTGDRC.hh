/*
 * This file is protected by Copyright. Please refer to the COPYRIGHT file
 * distributed with this source distribution.
 *
 * This file is part of OpenCPI <http://www.opencpi.org>
 *
 * OpenCPI is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _HTG_DRC_HH
#define _HTG_DRC_HH

#include "HDLDevicesRFDCDRC.hh"

/// @brief DEPENDENCIES: lmx2594_proxy.rcc worker
struct LMX2594ProxyCallBack {
  virtual void set_register(size_t addr, uint16_t val) = 0;
  virtual void set_in_filename(const std::string& val) = 0;
  virtual void start() = 0;
};

/// @brief DEPENDENCIES: HDLDevicesRFDCDRC class (DRC rfdc.hdl SUPPORT)
class HTGZRF848DRDRC : public HDLDevicesRFDCDRC {
  public:
  HTGZRF848DRDRC(RFDCCallBack& rfdc, LMX2594ProxyCallBack & lmx2594_proxy);
  void set_tics_pro_filename(const std::string& val);
  protected:
  std::string m_tics_pro_filename;
  LMX2594ProxyCallBack &m_lmx2594_proxy;
  void init_clock_source_devices();
  void init_lmx2594();
}; // class HTGZRF848DRDRC

#include "HTGDRC.cc"

#endif // _HTG_DRC_HH
