/*
 * This file is protected by Copyright. Please refer to the COPYRIGHT file
 * distributed with this source distribution.
 *
 * This file is part of OpenCPI <http://www.opencpi.org>
 *
 * OpenCPI is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _RFDC_DRC_HH
#define _RFDC_DRC_HH

#include "DRC.hh"
#include "xrfdc.h"
#include <stdint.h> // uint8_t

#include <iostream>
/// @TODO figure out why redfine is needed
#undef _LOG_INFO
#undef _LOG_DEBUG
#define _LOG_INFO(fmt, ...) printf("[INFO]  "); printf(fmt, ##__VA_ARGS__); printf("\n")
#define _LOG_DEBUG(fmt, ...) printf("[DEBUG] "); printf(fmt, ##__VA_ARGS__); printf("\n")

using namespace OCPI::DRC;

struct rfdc_ip_version_t {
  int major;
  int minor;
};

struct rfdc_api_info_for_drc_rf_port {
  u32 type;
  u32 tile;
  std::vector<u32> blocks;
};

/*! @brief DEPENDENCIES: rfdc.hdl worker,
 *                       rfdc_dac_0.hdl worker,
 *                       rfdc_dac_1.hdl worker,
 *                       rfdc_dac_2.hdl worker,
 *                       rfdc_dac_3.hdl worker,
 *                       rfdc_adc_0.hdl worker,
 *                       rfdc_adc_1.hdl worker,
 *                       rfdc_adc_2.hdl worker,
 *                       rfdc_adc_3.hdl worker */
struct RFDCCallBack {
  // from the low level rfdc/libmetal library
  virtual uint8_t get_uchar_prop(unsigned long of, unsigned long pof) = 0;
  virtual uint16_t get_ushort_prop(unsigned long of, unsigned long pof) = 0;
  virtual uint32_t get_ulong_prop(unsigned long of, unsigned long pof) = 0;
  virtual uint64_t get_ulonglong_prop(unsigned long of, unsigned long pof) = 0;
  virtual void set_uchar_prop(unsigned long of, unsigned long pof,
      uint8_t val) = 0;
  virtual void set_ushort_prop(unsigned long of, unsigned long pof,
      uint16_t val) = 0;
  virtual void set_ulong_prop(unsigned long of, unsigned long pof,
      uint32_t val) = 0;
  virtual void set_ulonglong_prop(unsigned long of, unsigned long pof,
      uint64_t val) = 0;
  virtual void take_rfdc_axi_lite_out_of_reset() = 0;
  virtual void take_rfdc_axi_stream_out_of_reset() = 0;
};

/*! @brief This class represents a Xilinx RF Data Converter (RFDC) with the
 *         generic DRC API. The rfdc software library is a dependency, with its
 *         in-tree libmetal library's linux system assumed patched to use
 *         struct DeviceCallBacks for its io accesses (this is because we are
 *         forcing the use of OpenCPI control plane for io).
 *         DEPENDENCIES:
 *           DRC class                      (DRC BASE SERVICES)
 *           rfdc OCPI prerequisite library (RFDC/LIBMETAL LIBRARY) */
class RFDCZU4XDRDRC : public DRC {
  public:
  RFDCZU4XDRDRC(RFDCCallBack &dev);
  void initialize(const std::vector<std::string>& rf_ports_rx,
      const std::vector<std::string>& rf_ports_tx);
  void dump_regs();
  protected:
  /*! @brief Variable Output Power from
   * "Table: RF-DAC Electrical Characteristics for ZU4xDR Devices" in
   * https://docs.xilinx.com/r/en-US/ds926-zynq-ultrascale-plus-rfsoc/RF-DAC-Electrical-Characteristics
   ****************************************************************************/
  struct vop_t {
    double current_step_size_ua = 43.75;
    // for set_point see shengjie response at
    // https://support.xilinx.com/s/question/0D54U00007axDsESAU/real-time-vop-configuration-is-not-changing-the-output-gain
    double set_point;
    double output_current_min_uamps;
    double output_current_max_uamps;
    double variable_output_power_range_db; // frequency-dependent!
  };
  bool m_initialized;
  /// @brief maps rf_port_name values to tile/block info in m_xrfdc_config
  std::map<std::string, rfdc_api_info_for_drc_rf_port> m_rf_port_infos;
  XRFdc_Config m_xrfdc_config[1];
  XRFdc m_xrfdc;
  RFDCCallBack& m_rfdc;
  bool m_enable_inverse_sinc_fir;
  bool get_rx(const std::string& rf_port_name);
  double get_tuning_freq_MHz(const std::string& rf_port_name);
  double get_bandwidth_3dB_MHz(const std::string& rf_port_name);
  double get_sampling_rate_Msps(const std::string& rf_port_name);
  bool get_samples_are_complex(const std::string& rf_port_name);
  std::string get_gain_mode(const std::string& rf_port_name);
  double get_gain_dB(const std::string& rf_port_name);
  OperatingPlan get_operating_plan_for_tuning_freq_MHz(
      const ConfigurationChannel& /*chan*/, const std::string& rf_port_name,
      double val, double tolerance);
  OperatingPlan get_operating_plan_for_bandwidth_3dB_MHz(
      const ConfigurationChannel& /*chan*/, const std::string& rf_port_name,
      double val, double tolerance);
  OperatingPlan get_operating_plan_for_sampling_rate_Msps(
      const ConfigurationChannel& /*chan*/, const std::string& rf_port_name,
      double val, double tolerance);
  OperatingPlan get_operating_plan_for_gain_dB(
      const ConfigurationChannel& /*chan*/, const std::string& rf_port_name,
      double val, double tolerance);
  OperatingPlan get_operating_plan_for_fs_bw(const ConfigurationChannel& chan,
      const std::string& rf_port_name, bool fs);
  rfdc_ip_version_t get_fpga_rfdc_ip_version();
  virtual int get_expected_ip_version_major() = 0;
  virtual int get_expected_ip_version_minor() = 0;
  u32 get_xrfdc_interp_decim_factor(double dfact);
  virtual std::vector<double> get_possible_interp_decim_vals(
      const std::string& rf_port_name);
  void set_rx(const std::string& rf_port_name, bool val);
  void set_tuning_freq_MHz(const std::string& rf_port_name, double val);
  void set_bandwidth_3dB_MHz(const std::string& rf_port_name, double val);
  void set_sampling_rate_Msps(const std::string& rf_port_name, double val);
  void set_samples_are_complex(const std::string& rf_port_name, bool val);
  void set_gain_mode(const std::string& rf_port_name, const std::string& val);
  void set_gain_dB(const std::string& rf_port_name, double val);
  void set_app_port_num(const std::string& rf_port_name, uint8_t val);
  void set_enable_inverse_sinc_fir(bool val);
  void set_datapath_mode(u32 tile, u32 block);
  virtual double access_tuning_freq_MHz(bool read, double val, u32 type, u32 tile,
      u32 block, double fs);
  virtual double access_bandwidth_3dB_MHz(bool read, double val, u32 type, u32 tile,
      u32 block);
  virtual double access_sampling_rate_Msps(bool read, double val, u32 type, u32 tile,
      u32 block);
  virtual double access_gain_dB(bool read, double val, u32 type, u32 tile, u32 block,
      double fc);
  virtual double access_radio_param(bool read, const std::string& rf_port_name,
      double val, int param);
  virtual double access_fs_bw(bool read, double default_val, double desired_val,
      u32 type, u32 tile, u32 block);
  void throw_if_proof_of_life_reg_test_fails();
  virtual void init_clock_source_devices() = 0;
  virtual void init_xrfdc_config() = 0;
  virtual void init_rf_port_infos() = 0;
  void init_metal();
  void log_xrfdc_status();
  virtual vop_t calc_vop(double fc);
  virtual u32 calc_vop_current_in_uamps(double fc, double desired_gain_db, bool apply_offset = true);
  virtual double calc_vop_gain_dB(double fc, double vop_uamps, bool apply_offset = true);
}; // class RFDCZU4XDRDRC

#include "RFDCDRC.cc"

#endif // _RFDC_DRC_HH
