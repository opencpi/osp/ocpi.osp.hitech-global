/*
 * This file is protected by Copyright. Please refer to the COPYRIGHT file
 * distributed with this source distribution.
 *
 * This file is part of OpenCPI <http://www.opencpi.org>
 *
 * OpenCPI is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _HDL_DEVICES_RFDC_DRC_HH
#define _HDL_DEVICES_RFDC_DRC_HH

#include "HDLPrimitivesRFDCDRC.hh"

/// @brief DEPENDENCIES: HDLPrimitivesRFDCDRC class (DRC rfdc primitive SUPPORT)
class HDLDevicesRFDCDRC : public HDLPrimitivesRFDCDRC {
  public:
  HDLDevicesRFDCDRC(RFDCCallBack& rfdc);
  protected:
  OperatingPlan get_operating_plan_for_app_port_num(
      const ConfigurationChannel& chan, const std::string& rf_port_name,
      double val);
  uint8_t get_app_port_num(const std::string& rf_port_name);
  std::vector<double> get_possible_interp_decim_vals(
      const std::string& rf_port_name);
}; // class HDLDevicesRFDCDRC

#include "HDLDevicesRFDCDRC.cc"

#endif // _HDL_DEVICES_RFDC_DRC_HH
