Everything in this directory is intended to eventually be merged into
opencpi.git/runtime/drc but will remain here until the core framework team
accepts it.

To run DRC base class unit test, run the following and confirm zero exit status:
    g++ -g -I. test.cc && ./a.out
