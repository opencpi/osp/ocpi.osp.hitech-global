/*
 * This file is protected by Copyright. Please refer to the COPYRIGHT file
 * distributed with this source distribution.
 *
 * This file is part of OpenCPI <http://www.opencpi.org>
 *
 * OpenCPI is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "HDLPrimitivesRFDCDRC.hh"

HDLPrimitivesRFDCDRC::HDLPrimitivesRFDCDRC(RFDCCallBack &rfdc) :
      RFDCZU4XDRDRC(rfdc),
    m_rfsoc_mixer_freq_constraint_mode(0) {
  m_description.assign("rfdchdlprimitive");
}

uint8_t
HDLPrimitivesRFDCDRC::get_rfsoc_mixer_freq_constraint_mode() {
  return m_rfsoc_mixer_freq_constraint_mode;
}

void
HDLPrimitivesRFDCDRC::initialize(
    const std::vector<std::string>& rf_ports_rx,
    const std::vector<std::string>& rf_ports_tx,
    const std::string& rf_port_name_rfdc_adc0,
    const std::string& rf_port_name_rfdc_adc2,
    const std::string& rf_port_name_rfdc_dac32,
    const std::string& rf_port_name_rfdc_dac30) {
  m_rf_port_name_rfdc_adc0.assign(rf_port_name_rfdc_adc0);
  m_rf_port_name_rfdc_adc2.assign(rf_port_name_rfdc_adc2);
  m_rf_port_name_rfdc_dac32.assign(rf_port_name_rfdc_dac32);
  m_rf_port_name_rfdc_dac30.assign(rf_port_name_rfdc_dac30);
  init_rf_port_infos();
  RFDCZU4XDRDRC::initialize(rf_ports_rx, rf_ports_tx);
}

void
HDLPrimitivesRFDCDRC::set_start(const uint16_t val) {
  DRC::set_start(val);
  if (m_status[val].state == state_t::operating) {
    auto it = m_status[val].channels.begin();
    for (; it != m_status[val].channels.end(); ++it) {
      const u32 ty = m_rf_port_infos.at(it->second.rf_port_name).type;
      const u32 tl = m_rf_port_infos.at(it->second.rf_port_name).tile;
      if (ty != XRFDC_ADC_TILE) {
        API_TYPE_AND_TILE_ONLY_WR(XRFdc_StartUp, &m_xrfdc, ty, tl)
      }
    }
  }
}

void
HDLPrimitivesRFDCDRC::set_stop(const uint16_t val) {
  DRC::set_stop(val);
  if (!get_any_dac_tile_is_operating()) {
    auto it = m_status[val].channels.begin();
    for (; it != m_status[val].channels.end(); ++it) {
      const u32 ty = m_rf_port_infos.at(it->second.rf_port_name).type;
      const u32 tl = m_rf_port_infos.at(it->second.rf_port_name).tile;
      if (ty != XRFDC_ADC_TILE) {
        API_TYPE_AND_TILE_ONLY_WR(XRFdc_Shutdown, &m_xrfdc, ty, tl)
      }
    }
  }
}

OperatingPlan
HDLPrimitivesRFDCDRC::get_operating_plan_for_rx(
    const ConfigurationChannel& /*chan*/, const std::string& rf_port_name,
    double val) {
  OperatingPlan ret;
  ret.val_to_set_to = val;
  if (this->m_rf_port_infos.at(rf_port_name).type == XRFDC_ADC_TILE) {
    if (val < 0.5) {
      ret.error.assign("tx is not supported");
    }
  }
  else {
    if (val > 0.5) {
      ret.error.assign("rx is not supported");
    }
  }
  return ret;
}

OperatingPlan
HDLPrimitivesRFDCDRC::get_operating_plan_for_tuning_freq_MHz(
    const ConfigurationChannel& chan, const std::string& rf_port_name,
    double val, double tolerance) {
  OperatingPlan ret;
  const auto& tol = tolerance;
  ret = RFDCZU4XDRDRC::get_operating_plan_for_tuning_freq_MHz(chan, rf_port_name, val, tol);
  if (ret.error.empty() && (m_rfsoc_mixer_freq_constraint_mode == 0)) {
    const bool adc = this->m_rf_port_infos.at(rf_port_name).type == XRFDC_ADC_TILE;
    // optimized range
    double min = 180.;
    double max = adc ? 405. : 855.;
    ret = get_operating_plan_continuous_limits(val, min, max, tol);
  }
  return ret;
}

OperatingPlan
HDLPrimitivesRFDCDRC::get_operating_plan_for_samples_are_complex(
      const ConfigurationChannel& /*chan*/, const std::string& /*rf_port_name*/,
      double val) {
  return get_operating_plan_complex_samples_only(val);
}

OperatingPlan
HDLPrimitivesRFDCDRC::get_operating_plan_for_gain_mode(
    const ConfigurationChannel& /*chan*/, const std::string& /*rf_port_name*/,
    double val) {
  return get_operating_plan_manual_gain_mode_only(val);
}

int
HDLPrimitivesRFDCDRC::get_expected_ip_version_major() {
  return 2;
}

int
HDLPrimitivesRFDCDRC::get_expected_ip_version_minor() {
  return 5;
}

bool
HDLPrimitivesRFDCDRC::get_any_dac_tile_is_operating() {
  bool dac32_operating = false;
  bool dac30_operating = false;
  for (auto it2 = m_status.begin(); it2 != m_status.end(); ++it2) {
    if (it2->second.state == state_t::operating) {
      auto it = it2->second.channels.begin();
      for (; it != it2->second.channels.end(); ++it) {
        if (!it->second.rf_port_name.compare(m_rf_port_name_rfdc_dac32)) {
          dac32_operating = true;
        }
        if (!it->second.rf_port_name.compare(m_rf_port_name_rfdc_dac30)) {
          dac30_operating = true;
        }
      }
    }
  }
  return (dac32_operating || dac30_operating);
}

void
HDLPrimitivesRFDCDRC::init_xrfdc_config() {
  // below line is necessary to get XRFdc_SetDecimationFactor to work properly
  this->m_xrfdc_config[0].IPType = XRFDC_GEN3;
  this->m_xrfdc_config[0].SiRevision = 1;
  // below line is necessary to get XRFdc_SetClkDistribution to work properly
  this->m_xrfdc_config[0].ADCType = 1;
  // Enable, RefClkFreq, and SamplingRate are necessary to get
  // SetMixerSettings to work properly
  auto& cfg = this->m_xrfdc_config[0];
  cfg.ADCTile_Config[0].Enable = 1;
  cfg.ADCTile_Config[0].RefClkFreq = 3.6;
  cfg.ADCTile_Config[0].SamplingRate = 3.6;
  cfg.ADCTile_Config[2].Enable = 1;
  cfg.ADCTile_Config[2].RefClkFreq = 3.6;
  cfg.ADCTile_Config[2].SamplingRate = 3.6;
  cfg.DACTile_Config[3].Enable = 1;
  cfg.DACTile_Config[3].RefClkFreq = 3.6;
  cfg.DACTile_Config[3].SamplingRate = 3.6;
  cfg.ADCTile_Config[0].ADCBlock_Digital_Config[0].DecimationMode = 1;
  cfg.ADCTile_Config[0].ADCBlock_Digital_Config[1].DecimationMode = 1;
  cfg.ADCTile_Config[0].ADCBlock_Digital_Config[2].DecimationMode = 1;
  cfg.ADCTile_Config[0].ADCBlock_Digital_Config[3].DecimationMode = 1;
  cfg.ADCTile_Config[2].ADCBlock_Digital_Config[0].DecimationMode = 1;
  cfg.ADCTile_Config[2].ADCBlock_Digital_Config[1].DecimationMode = 1;
  cfg.ADCTile_Config[2].ADCBlock_Digital_Config[2].DecimationMode = 1;
  cfg.ADCTile_Config[2].ADCBlock_Digital_Config[3].DecimationMode = 1;
  cfg.DACTile_Config[3].DACBlock_Digital_Config[0].InterpolationMode = 1;
  cfg.DACTile_Config[3].DACBlock_Digital_Config[1].InterpolationMode = 1;
  cfg.DACTile_Config[3].DACBlock_Digital_Config[2].InterpolationMode = 1;
  cfg.DACTile_Config[3].DACBlock_Digital_Config[3].InterpolationMode = 1;
  // below line from opencpi-modified libmetal linux layer
  this->m_xrfdc.io = &metal_io_region_;
}

void
HDLPrimitivesRFDCDRC::init_rf_port_infos() {
  rfdc_api_info_for_drc_rf_port tmp;
  tmp.type = XRFDC_ADC_TILE;
  tmp.tile = 0;
  tmp.blocks = {0, 1};
  this->m_rf_port_infos.emplace(m_rf_port_name_rfdc_adc0, tmp);
  tmp.type = XRFDC_ADC_TILE;
  tmp.tile = 2;
  tmp.blocks = {0, 1};
  this->m_rf_port_infos.emplace(m_rf_port_name_rfdc_adc2, tmp);
  tmp.type = XRFDC_DAC_TILE;
  tmp.tile = 3;
  tmp.blocks = {2};
  this->m_rf_port_infos.emplace(m_rf_port_name_rfdc_dac32, tmp);
  tmp.type = XRFDC_DAC_TILE;
  tmp.tile = 3;
  tmp.blocks = {0};
  this->m_rf_port_infos.emplace(m_rf_port_name_rfdc_dac30, tmp);
}
