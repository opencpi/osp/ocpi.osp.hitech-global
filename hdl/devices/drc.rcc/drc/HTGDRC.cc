/*
 * This file is protected by Copyright. Please refer to the COPYRIGHT file
 * distributed with this source distribution.
 *
 * This file is part of OpenCPI <http://www.opencpi.org>
 *
 * OpenCPI is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "HTGDRC.hh"

HTGZRF848DRDRC::HTGZRF848DRDRC(RFDCCallBack& rfdc,
    LMX2594ProxyCallBack& lmx2594_proxy) : HDLDevicesRFDCDRC(rfdc),
    m_lmx2594_proxy(lmx2594_proxy) {
  m_description.assign("zrf848dr");
}

void
HTGZRF848DRDRC::set_tics_pro_filename(const std::string& val) {
  m_tics_pro_filename.assign(val);
}

void
HTGZRF848DRDRC::init_clock_source_devices() {
  init_lmx2594();
}

/*! @brief This not only sets the LMX 2594 (and, effectively, the ZRF8 RFDC
 *         input) clock rate, but sets a whole slew of other settings - see
 *         TICSPRO-SW or the LMX datasheet for more info. If
 *         set_tics_pro_filename() is already
 *         called with a non-empty value, the config file is used to determine
 *         the clock rate (meant for debugging purposes), otherwise this
 *         sets the rate which the ZRF8 RFDC is currently designed for.
 ******************************************************************************/
void
HTGZRF848DRDRC::init_lmx2594() {
  if (m_tics_pro_filename.empty()) {
    m_lmx2594_proxy.set_register(0x70, 0x0000);
    m_lmx2594_proxy.set_register(0x6F, 0x0000);
    m_lmx2594_proxy.set_register(0x6E, 0x0000);
    m_lmx2594_proxy.set_register(0x6D, 0x0000);
    m_lmx2594_proxy.set_register(0x6C, 0x0000);
    m_lmx2594_proxy.set_register(0x6B, 0x0000);
    m_lmx2594_proxy.set_register(0x6A, 0x0000);
    m_lmx2594_proxy.set_register(0x69, 0x0021);
    m_lmx2594_proxy.set_register(0x68, 0x0000);
    m_lmx2594_proxy.set_register(0x67, 0x0000);
    m_lmx2594_proxy.set_register(0x66, 0x0000);
    m_lmx2594_proxy.set_register(0x65, 0x0011);
    m_lmx2594_proxy.set_register(0x64, 0x0000);
    m_lmx2594_proxy.set_register(0x63, 0x0000);
    m_lmx2594_proxy.set_register(0x62, 0x0000);
    m_lmx2594_proxy.set_register(0x61, 0x0888);
    m_lmx2594_proxy.set_register(0x60, 0x0000);
    m_lmx2594_proxy.set_register(0x5F, 0x0000);
    m_lmx2594_proxy.set_register(0x5E, 0x0000);
    m_lmx2594_proxy.set_register(0x5D, 0x0000);
    m_lmx2594_proxy.set_register(0x5C, 0x0000);
    m_lmx2594_proxy.set_register(0x5B, 0x0000);
    m_lmx2594_proxy.set_register(0x5A, 0x0000);
    m_lmx2594_proxy.set_register(0x59, 0x0000);
    m_lmx2594_proxy.set_register(0x58, 0x0000);
    m_lmx2594_proxy.set_register(0x57, 0x0000);
    m_lmx2594_proxy.set_register(0x56, 0x0000);
    m_lmx2594_proxy.set_register(0x55, 0x0000);
    m_lmx2594_proxy.set_register(0x54, 0x0000);
    m_lmx2594_proxy.set_register(0x53, 0x0000);
    m_lmx2594_proxy.set_register(0x52, 0x0000);
    m_lmx2594_proxy.set_register(0x51, 0x0000);
    m_lmx2594_proxy.set_register(0x50, 0x0000);
    m_lmx2594_proxy.set_register(0x4F, 0x0000);
    m_lmx2594_proxy.set_register(0x4E, 0x0003);
    m_lmx2594_proxy.set_register(0x4D, 0x0000);
    m_lmx2594_proxy.set_register(0x4C, 0x000C);
    m_lmx2594_proxy.set_register(0x4B, 0x0840);
    m_lmx2594_proxy.set_register(0x4A, 0x0000);
    m_lmx2594_proxy.set_register(0x49, 0x003F);
    m_lmx2594_proxy.set_register(0x48, 0x0001);
    m_lmx2594_proxy.set_register(0x47, 0x0081);
    m_lmx2594_proxy.set_register(0x46, 0xC350);
    m_lmx2594_proxy.set_register(0x45, 0x0000);
    m_lmx2594_proxy.set_register(0x44, 0x03E8);
    m_lmx2594_proxy.set_register(0x43, 0x0000);
    m_lmx2594_proxy.set_register(0x42, 0x01F4);
    m_lmx2594_proxy.set_register(0x41, 0x0000);
    m_lmx2594_proxy.set_register(0x40, 0x1388);
    m_lmx2594_proxy.set_register(0x3F, 0x0000);
    m_lmx2594_proxy.set_register(0x3E, 0x0322);
    m_lmx2594_proxy.set_register(0x3D, 0x00A8);
    m_lmx2594_proxy.set_register(0x3C, 0x0000);
    m_lmx2594_proxy.set_register(0x3B, 0x0001);
    m_lmx2594_proxy.set_register(0x3A, 0x9001);
    m_lmx2594_proxy.set_register(0x39, 0x0020);
    m_lmx2594_proxy.set_register(0x38, 0x0000);
    m_lmx2594_proxy.set_register(0x37, 0x0000);
    m_lmx2594_proxy.set_register(0x36, 0x0000);
    m_lmx2594_proxy.set_register(0x35, 0x0000);
    m_lmx2594_proxy.set_register(0x34, 0x0820);
    m_lmx2594_proxy.set_register(0x33, 0x0080);
    m_lmx2594_proxy.set_register(0x32, 0x0000);
    m_lmx2594_proxy.set_register(0x31, 0x4180);
    m_lmx2594_proxy.set_register(0x30, 0x0300);
    m_lmx2594_proxy.set_register(0x2F, 0x0300);
    m_lmx2594_proxy.set_register(0x2E, 0x07FC);
    m_lmx2594_proxy.set_register(0x2D, 0xC0DF);
    m_lmx2594_proxy.set_register(0x2C, 0x1F22);
    m_lmx2594_proxy.set_register(0x2B, 0x0000);
    m_lmx2594_proxy.set_register(0x2A, 0x0000);
    m_lmx2594_proxy.set_register(0x29, 0x0000);
    m_lmx2594_proxy.set_register(0x28, 0x0000);
    m_lmx2594_proxy.set_register(0x27, 0xFFFF);
    m_lmx2594_proxy.set_register(0x26, 0xFFFF);
    m_lmx2594_proxy.set_register(0x25, 0x0304);
    m_lmx2594_proxy.set_register(0x24, 0x0048);
    m_lmx2594_proxy.set_register(0x23, 0x0004);
    m_lmx2594_proxy.set_register(0x22, 0x0000);
    m_lmx2594_proxy.set_register(0x21, 0x1E21);
    m_lmx2594_proxy.set_register(0x20, 0x0393);
    m_lmx2594_proxy.set_register(0x1F, 0x43EC);
    m_lmx2594_proxy.set_register(0x1E, 0x318C);
    m_lmx2594_proxy.set_register(0x1D, 0x318C);
    m_lmx2594_proxy.set_register(0x1C, 0x0488);
    m_lmx2594_proxy.set_register(0x1B, 0x0002);
    m_lmx2594_proxy.set_register(0x1A, 0x0DB0);
    m_lmx2594_proxy.set_register(0x19, 0x0C2B);
    m_lmx2594_proxy.set_register(0x18, 0x071A);
    m_lmx2594_proxy.set_register(0x17, 0x007C);
    m_lmx2594_proxy.set_register(0x16, 0x0001);
    m_lmx2594_proxy.set_register(0x15, 0x0401);
    m_lmx2594_proxy.set_register(0x14, 0xF848);
    m_lmx2594_proxy.set_register(0x13, 0x27B7);
    m_lmx2594_proxy.set_register(0x12, 0x0064);
    m_lmx2594_proxy.set_register(0x11, 0x012C);
    m_lmx2594_proxy.set_register(0x10, 0x0080);
    m_lmx2594_proxy.set_register(0x0F, 0x064F);
    m_lmx2594_proxy.set_register(0x0E, 0x1E70);
    m_lmx2594_proxy.set_register(0x0D, 0x4000);
    m_lmx2594_proxy.set_register(0x0C, 0x5001);
    m_lmx2594_proxy.set_register(0x0B, 0x0018);
    m_lmx2594_proxy.set_register(0x0A, 0x10D8);
    m_lmx2594_proxy.set_register(0x09, 0x1604);
    m_lmx2594_proxy.set_register(0x08, 0x2000);
    m_lmx2594_proxy.set_register(0x07, 0x40B2);
    m_lmx2594_proxy.set_register(0x06, 0xC802);
    m_lmx2594_proxy.set_register(0x05, 0x00C8);
    m_lmx2594_proxy.set_register(0x04, 0x0C43);
    m_lmx2594_proxy.set_register(0x03, 0x0642);
    m_lmx2594_proxy.set_register(0x02, 0x0500);
    m_lmx2594_proxy.set_register(0x01, 0x0808);
    m_lmx2594_proxy.set_register(0x00, /*0x251C*/ 0x2518);
  }
  else {
    auto& filename = m_tics_pro_filename;
    m_lmx2594_proxy.set_in_filename(filename.c_str());
  }
  m_lmx2594_proxy.start();
}
