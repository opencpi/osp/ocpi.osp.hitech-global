/*
 * This file is protected by Copyright. Please refer to the COPYRIGHT file
 * distributed with this source distribution.
 *
 * This file is part of OpenCPI <http://www.opencpi.org>
 *
 * OpenCPI is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

HDLDevicesRFDCDRC::HDLDevicesRFDCDRC(RFDCCallBack& rfdc) :
    HDLPrimitivesRFDCDRC(rfdc) {
  m_description.assign("rfdchdldevice");
}

OperatingPlan
HDLDevicesRFDCDRC::get_operating_plan_for_app_port_num(
    const ConfigurationChannel& /*chan*/, const std::string& rf_port_name,
    double val) {
  OperatingPlan ret;
  ret.val_to_set_to = val;
  if (val > 0.5) {
    bool adc0 = !rf_port_name.compare(m_rf_port_name_rfdc_adc0);
    if (adc0 || (!rf_port_name.compare(m_rf_port_name_rfdc_dac30))) {
      ret.error.assign("app port num 1 is not supported");
    }
  }
  if (val < 0.5) {
    bool adc2 = !rf_port_name.compare(m_rf_port_name_rfdc_adc2);
    if (adc2 || (!rf_port_name.compare(m_rf_port_name_rfdc_dac32))) {
      ret.error.assign("app port num 0 is not supported");
    }
  }
  return ret;
}

uint8_t
HDLDevicesRFDCDRC::get_app_port_num(const std::string& rf_port_name) {
  uint8_t ret;
  if (!rf_port_name.compare(m_rf_port_name_rfdc_adc0)) {
    ret = 0;
  }
  else if (!rf_port_name.compare(m_rf_port_name_rfdc_adc2)) {
    ret = 1;
  }
  else if (!rf_port_name.compare(m_rf_port_name_rfdc_dac30)) {
    ret = 0;
  }
  else {
    ret = 1;
  }
  return ret;
}

/* @brief this is limited to less than full range of RFSoc due to wsi port
 * clock constraints */
std::vector<double>
HDLDevicesRFDCDRC::get_possible_interp_decim_vals(const std::string& /*rf_port_name*/) {
  return {40};
}
