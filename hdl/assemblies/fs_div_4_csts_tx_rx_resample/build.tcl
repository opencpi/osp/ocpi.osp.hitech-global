# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public License along
# with this program. If not, see <http://www.gnu.org/licenses/>.

set xci_path "../../../../primitives/rfdc/gen/tmp/managed_ip_project/managed_ip_project.srcs/sources_1/ip/usp_rf_data_converter_0/usp_rf_data_converter_0.xci"
add_files -norecurse "$xci_path"
export_ip_user_files -of_objects [get_files "$xci_path"] -force -quiet
set clk_xci_path "../../../../primitives/rfdc/gen/tmp/managed_ip_project/managed_ip_project.srcs/sources_1/ip/clk_wiz_0/clk_wiz_0.xci"
add_files -norecurse "$clk_xci_path"
export_ip_user_files -of_objects [get_files "$clk_xci_path"] -force -quiet
# the below line is modified per assembly/config/container combo, and the format of the property value is <assembly_<platform>_<pfconfig>_<container> (remove trailing _<container> if default container is used)
set_property top fs_div_4_csts_tx_rx_resample_zrf8_48dr_cfg_rfdc_j3_j13_j18_j20_cnt [current_fileset]
link_design -name netlist_1
# comment out below line to add useful print of available clock names
#puts [all_clocks]
set_false_path -from [get_clocks clk_fpga_0] -to [get_clocks RFADC0_CLK]
set_false_path -from [get_clocks clk_fpga_0] -to [get_clocks RFADC1_CLK]
set_false_path -from [get_clocks clk_fpga_0] -to [get_clocks RFADC2_CLK]
set_false_path -from [get_clocks clk_fpga_0] -to [get_clocks RFADC3_CLK]
set_false_path -from [get_clocks clk_fpga_0] -to [get_clocks RFDAC1_CLK]
set_false_path -from [get_clocks clk_fpga_0] -to [get_clocks RFDAC2_CLK]
set_false_path -from [get_clocks clk_fpga_0] -to [get_clocks RFDAC3_CLK]
set_false_path -from [get_clocks clk_fpga_0] -to [get_clocks clk_out1_clk_wiz_0_1]
set_false_path -from [get_clocks clk_out1_clk_wiz_0_1] -to [get_clocks RFADC0_CLK]
set_false_path -from [get_clocks clk_out1_clk_wiz_0_1] -to [get_clocks RFADC1_CLK]
set_false_path -from [get_clocks clk_out1_clk_wiz_0_1] -to [get_clocks RFADC2_CLK]
set_false_path -from [get_clocks clk_out1_clk_wiz_0_1] -to [get_clocks RFADC3_CLK]
set_false_path -from [get_clocks clk_out1_clk_wiz_0_1] -to [get_clocks RFDAC1_CLK]
set_false_path -from [get_clocks clk_out1_clk_wiz_0_1] -to [get_clocks RFDAC2_CLK]
set_false_path -from [get_clocks clk_out1_clk_wiz_0_1] -to [get_clocks RFDAC3_CLK]
set_false_path -from [get_clocks clk_out1_clk_wiz_0_1] -to [get_clocks clk_fpga_0]
set_false_path -from [get_clocks RFADC0_CLK] -to [get_clocks clk_fpga_0]
set_false_path -from [get_clocks RFADC1_CLK] -to [get_clocks clk_fpga_0]
set_false_path -from [get_clocks RFADC2_CLK] -to [get_clocks clk_fpga_0]
set_false_path -from [get_clocks RFADC3_CLK] -to [get_clocks clk_fpga_0]
set_false_path -from [get_clocks RFDAC1_CLK] -to [get_clocks clk_fpga_0]
set_false_path -from [get_clocks RFDAC2_CLK] -to [get_clocks clk_fpga_0]
set_false_path -from [get_clocks RFDAC3_CLK] -to [get_clocks clk_fpga_0]
set_false_path -from [get_clocks RFADC0_CLK] -to [get_clocks clk_out1_clk_wiz_0_1]
set_false_path -from [get_clocks RFADC1_CLK] -to [get_clocks clk_out1_clk_wiz_0_1]
set_false_path -from [get_clocks RFADC2_CLK] -to [get_clocks clk_out1_clk_wiz_0_1]
set_false_path -from [get_clocks RFADC3_CLK] -to [get_clocks clk_out1_clk_wiz_0_1]
set_false_path -from [get_clocks RFDAC1_CLK] -to [get_clocks clk_out1_clk_wiz_0_1]
set_false_path -from [get_clocks RFDAC2_CLK] -to [get_clocks clk_out1_clk_wiz_0_1]
set_false_path -from [get_clocks RFDAC3_CLK] -to [get_clocks clk_out1_clk_wiz_0_1]

#create_debug_core u_ila_0 ila
#set_property C_DATA_DEPTH 8192 [get_debug_cores u_ila_0]
#set_property C_TRIGIN_EN false [get_debug_cores u_ila_0]
#set_property C_TRIGOUT_EN false [get_debug_cores u_ila_0]
#set_property C_ADV_TRIGGER false [get_debug_cores u_ila_0]
#set_property C_INPUT_PIPE_STAGES 0 [get_debug_cores u_ila_0]
#set_property C_EN_STRG_QUAL false [get_debug_cores u_ila_0]
#set_property ALL_PROBE_SAME_MU true [get_debug_cores u_ila_0]
#set_property ALL_PROBE_SAME_MU_CNT 1 [get_debug_cores u_ila_0]
#startgroup 
#set_property C_EN_STRG_QUAL true [get_debug_cores u_ila_0 ]
#set_property C_ADV_TRIGGER true [get_debug_cores u_ila_0 ]
#set_property ALL_PROBE_SAME_MU true [get_debug_cores u_ila_0 ]
#set_property ALL_PROBE_SAME_MU_CNT 4 [get_debug_cores u_ila_0 ]
#endgroup
#
#create_debug_core u_ila_1 ila
#set_property C_DATA_DEPTH 8192 [get_debug_cores u_ila_1]
#set_property C_TRIGIN_EN false [get_debug_cores u_ila_1]
#set_property C_TRIGOUT_EN false [get_debug_cores u_ila_1]
#set_property C_ADV_TRIGGER false [get_debug_cores u_ila_1]
#set_property C_INPUT_PIPE_STAGES 0 [get_debug_cores u_ila_1]
#set_property C_EN_STRG_QUAL false [get_debug_cores u_ila_1]
#set_property ALL_PROBE_SAME_MU true [get_debug_cores u_ila_1]
#set_property ALL_PROBE_SAME_MU_CNT 1 [get_debug_cores u_ila_1]
#startgroup 
#set_property C_EN_STRG_QUAL true [get_debug_cores u_ila_1 ]
#set_property C_ADV_TRIGGER true [get_debug_cores u_ila_1 ]
#set_property ALL_PROBE_SAME_MU true [get_debug_cores u_ila_1 ]
#set_property ALL_PROBE_SAME_MU_CNT 4 [get_debug_cores u_ila_1 ]
#endgroup
#connect_debug_port u_ila_0/clk [get_nets [list ftop/pfconfig_i/zrf8_48dr_i/worker/ps/U0/U0/pl_clk0 ]]
#connect_debug_port u_ila_1/clk [get_nets [list ftop/pfconfig_i/rfdc_i/worker/rfdc_prim/clock_converter/inst/clk_out1 ]]
#set_property port_width 30 [get_debug_ports u_ila_0/probe0]
#set_property PROBE_TYPE DATA [get_debug_ports u_ila_0/probe0]
#connect_debug_port u_ila_0/probe0 [get_nets [list {ftop/pfconfig_i/rfdc_i/worker/ctl_rx_0_fifo/count[0]} {ftop/pfconfig_i/rfdc_i/worker/ctl_rx_0_fifo/count[1]} {ftop/pfconfig_i/rfdc_i/worker/ctl_rx_0_fifo/count[2]} {ftop/pfconfig_i/rfdc_i/worker/ctl_rx_0_fifo/count[3]} {ftop/pfconfig_i/rfdc_i/worker/ctl_rx_0_fifo/count[4]} {ftop/pfconfig_i/rfdc_i/worker/ctl_rx_0_fifo/count[5]} {ftop/pfconfig_i/rfdc_i/worker/ctl_rx_0_fifo/count[6]} {ftop/pfconfig_i/rfdc_i/worker/ctl_rx_0_fifo/count[7]} {ftop/pfconfig_i/rfdc_i/worker/ctl_rx_0_fifo/count[8]} {ftop/pfconfig_i/rfdc_i/worker/ctl_rx_0_fifo/count[9]} {ftop/pfconfig_i/rfdc_i/worker/ctl_rx_0_fifo/count[10]} {ftop/pfconfig_i/rfdc_i/worker/ctl_rx_0_fifo/count[11]} {ftop/pfconfig_i/rfdc_i/worker/ctl_rx_0_fifo/count[12]} {ftop/pfconfig_i/rfdc_i/worker/ctl_rx_0_fifo/count[13]} {ftop/pfconfig_i/rfdc_i/worker/ctl_rx_0_fifo/count[14]} {ftop/pfconfig_i/rfdc_i/worker/ctl_rx_0_fifo/count[15]} {ftop/pfconfig_i/rfdc_i/worker/ctl_rx_0_fifo/count[16]} {ftop/pfconfig_i/rfdc_i/worker/ctl_rx_0_fifo/count[17]} {ftop/pfconfig_i/rfdc_i/worker/ctl_rx_0_fifo/count[18]} {ftop/pfconfig_i/rfdc_i/worker/ctl_rx_0_fifo/count[19]} {ftop/pfconfig_i/rfdc_i/worker/ctl_rx_0_fifo/count[20]} {ftop/pfconfig_i/rfdc_i/worker/ctl_rx_0_fifo/count[21]} {ftop/pfconfig_i/rfdc_i/worker/ctl_rx_0_fifo/count[22]} {ftop/pfconfig_i/rfdc_i/worker/ctl_rx_0_fifo/count[23]} {ftop/pfconfig_i/rfdc_i/worker/ctl_rx_0_fifo/count[24]} {ftop/pfconfig_i/rfdc_i/worker/ctl_rx_0_fifo/count[25]} {ftop/pfconfig_i/rfdc_i/worker/ctl_rx_0_fifo/count[26]} {ftop/pfconfig_i/rfdc_i/worker/ctl_rx_0_fifo/count[27]} {ftop/pfconfig_i/rfdc_i/worker/ctl_rx_0_fifo/count[28]} {ftop/pfconfig_i/rfdc_i/worker/ctl_rx_0_fifo/count[29]} ]]
#create_debug_port u_ila_0 probe
#set_property port_width 16 [get_debug_ports u_ila_0/probe1]
#set_property PROBE_TYPE DATA [get_debug_ports u_ila_0/probe1]
#connect_debug_port u_ila_0/probe1 [get_nets [list {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/data_out_q[0]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/data_out_q[1]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/data_out_q[2]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/data_out_q[3]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/data_out_q[4]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/data_out_q[5]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/data_out_q[6]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/data_out_q[7]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/data_out_q[8]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/data_out_q[9]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/data_out_q[10]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/data_out_q[11]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/data_out_q[12]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/data_out_q[13]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/data_out_q[14]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/data_out_q[15]} ]]
#create_debug_port u_ila_0 probe
#set_property port_width 16 [get_debug_ports u_ila_0/probe2]
#set_property PROBE_TYPE DATA [get_debug_ports u_ila_0/probe2]
#connect_debug_port u_ila_0/probe2 [get_nets [list {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/data_out_i[0]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/data_out_i[1]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/data_out_i[2]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/data_out_i[3]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/data_out_i[4]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/data_out_i[5]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/data_out_i[6]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/data_out_i[7]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/data_out_i[8]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/data_out_i[9]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/data_out_i[10]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/data_out_i[11]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/data_out_i[12]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/data_out_i[13]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/data_out_i[14]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/data_out_i[15]} ]]
#create_debug_port u_ila_0 probe
#set_property port_width 32 [get_debug_ports u_ila_0/probe3]
#set_property PROBE_TYPE DATA [get_debug_ports u_ila_0/probe3]
#connect_debug_port u_ila_0/probe3 [get_nets [list {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/input_data[0]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/input_data[1]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/input_data[2]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/input_data[3]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/input_data[4]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/input_data[5]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/input_data[6]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/input_data[7]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/input_data[8]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/input_data[9]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/input_data[10]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/input_data[11]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/input_data[12]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/input_data[13]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/input_data[14]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/input_data[15]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/input_data[16]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/input_data[17]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/input_data[18]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/input_data[19]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/input_data[20]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/input_data[21]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/input_data[22]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/input_data[23]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/input_data[24]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/input_data[25]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/input_data[26]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/input_data[27]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/input_data[28]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/input_data[29]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/input_data[30]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/input_data[31]} ]]
#create_debug_port u_ila_0 probe
#set_property port_width 1 [get_debug_ports u_ila_0/probe4]
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe4]
#connect_debug_port u_ila_0/probe4 [get_nets [list {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/input_SThreadBusy[0]} ]]
#create_debug_port u_ila_0 probe
#set_property port_width 32 [get_debug_ports u_ila_0/probe5]
#set_property PROBE_TYPE DATA [get_debug_ports u_ila_0/probe5]
#connect_debug_port u_ila_0/probe5 [get_nets [list {ftop/pfconfig_i/rfdc_i/worker/ctl_tx_0_demarshaller_to_tx_0_cdc_tdata[0]} {ftop/pfconfig_i/rfdc_i/worker/ctl_tx_0_demarshaller_to_tx_0_cdc_tdata[1]} {ftop/pfconfig_i/rfdc_i/worker/ctl_tx_0_demarshaller_to_tx_0_cdc_tdata[2]} {ftop/pfconfig_i/rfdc_i/worker/ctl_tx_0_demarshaller_to_tx_0_cdc_tdata[3]} {ftop/pfconfig_i/rfdc_i/worker/ctl_tx_0_demarshaller_to_tx_0_cdc_tdata[4]} {ftop/pfconfig_i/rfdc_i/worker/ctl_tx_0_demarshaller_to_tx_0_cdc_tdata[5]} {ftop/pfconfig_i/rfdc_i/worker/ctl_tx_0_demarshaller_to_tx_0_cdc_tdata[6]} {ftop/pfconfig_i/rfdc_i/worker/ctl_tx_0_demarshaller_to_tx_0_cdc_tdata[7]} {ftop/pfconfig_i/rfdc_i/worker/ctl_tx_0_demarshaller_to_tx_0_cdc_tdata[8]} {ftop/pfconfig_i/rfdc_i/worker/ctl_tx_0_demarshaller_to_tx_0_cdc_tdata[9]} {ftop/pfconfig_i/rfdc_i/worker/ctl_tx_0_demarshaller_to_tx_0_cdc_tdata[10]} {ftop/pfconfig_i/rfdc_i/worker/ctl_tx_0_demarshaller_to_tx_0_cdc_tdata[11]} {ftop/pfconfig_i/rfdc_i/worker/ctl_tx_0_demarshaller_to_tx_0_cdc_tdata[12]} {ftop/pfconfig_i/rfdc_i/worker/ctl_tx_0_demarshaller_to_tx_0_cdc_tdata[13]} {ftop/pfconfig_i/rfdc_i/worker/ctl_tx_0_demarshaller_to_tx_0_cdc_tdata[14]} {ftop/pfconfig_i/rfdc_i/worker/ctl_tx_0_demarshaller_to_tx_0_cdc_tdata[15]} {ftop/pfconfig_i/rfdc_i/worker/ctl_tx_0_demarshaller_to_tx_0_cdc_tdata[16]} {ftop/pfconfig_i/rfdc_i/worker/ctl_tx_0_demarshaller_to_tx_0_cdc_tdata[17]} {ftop/pfconfig_i/rfdc_i/worker/ctl_tx_0_demarshaller_to_tx_0_cdc_tdata[18]} {ftop/pfconfig_i/rfdc_i/worker/ctl_tx_0_demarshaller_to_tx_0_cdc_tdata[19]} {ftop/pfconfig_i/rfdc_i/worker/ctl_tx_0_demarshaller_to_tx_0_cdc_tdata[20]} {ftop/pfconfig_i/rfdc_i/worker/ctl_tx_0_demarshaller_to_tx_0_cdc_tdata[21]} {ftop/pfconfig_i/rfdc_i/worker/ctl_tx_0_demarshaller_to_tx_0_cdc_tdata[22]} {ftop/pfconfig_i/rfdc_i/worker/ctl_tx_0_demarshaller_to_tx_0_cdc_tdata[23]} {ftop/pfconfig_i/rfdc_i/worker/ctl_tx_0_demarshaller_to_tx_0_cdc_tdata[24]} {ftop/pfconfig_i/rfdc_i/worker/ctl_tx_0_demarshaller_to_tx_0_cdc_tdata[25]} {ftop/pfconfig_i/rfdc_i/worker/ctl_tx_0_demarshaller_to_tx_0_cdc_tdata[26]} {ftop/pfconfig_i/rfdc_i/worker/ctl_tx_0_demarshaller_to_tx_0_cdc_tdata[27]} {ftop/pfconfig_i/rfdc_i/worker/ctl_tx_0_demarshaller_to_tx_0_cdc_tdata[28]} {ftop/pfconfig_i/rfdc_i/worker/ctl_tx_0_demarshaller_to_tx_0_cdc_tdata[29]} {ftop/pfconfig_i/rfdc_i/worker/ctl_tx_0_demarshaller_to_tx_0_cdc_tdata[30]} {ftop/pfconfig_i/rfdc_i/worker/ctl_tx_0_demarshaller_to_tx_0_cdc_tdata[31]} ]]
#create_debug_port u_ila_0 probe
#set_property port_width 32 [get_debug_ports u_ila_0/probe6]
#set_property PROBE_TYPE DATA [get_debug_ports u_ila_0/probe6]
#connect_debug_port u_ila_0/probe6 [get_nets [list {ftop/pfconfig_i/rfdc_i/worker/ctl_rx_0_fifo_to_rx_0_marshaller_tdata[0]} {ftop/pfconfig_i/rfdc_i/worker/ctl_rx_0_fifo_to_rx_0_marshaller_tdata[1]} {ftop/pfconfig_i/rfdc_i/worker/ctl_rx_0_fifo_to_rx_0_marshaller_tdata[2]} {ftop/pfconfig_i/rfdc_i/worker/ctl_rx_0_fifo_to_rx_0_marshaller_tdata[3]} {ftop/pfconfig_i/rfdc_i/worker/ctl_rx_0_fifo_to_rx_0_marshaller_tdata[4]} {ftop/pfconfig_i/rfdc_i/worker/ctl_rx_0_fifo_to_rx_0_marshaller_tdata[5]} {ftop/pfconfig_i/rfdc_i/worker/ctl_rx_0_fifo_to_rx_0_marshaller_tdata[6]} {ftop/pfconfig_i/rfdc_i/worker/ctl_rx_0_fifo_to_rx_0_marshaller_tdata[7]} {ftop/pfconfig_i/rfdc_i/worker/ctl_rx_0_fifo_to_rx_0_marshaller_tdata[8]} {ftop/pfconfig_i/rfdc_i/worker/ctl_rx_0_fifo_to_rx_0_marshaller_tdata[9]} {ftop/pfconfig_i/rfdc_i/worker/ctl_rx_0_fifo_to_rx_0_marshaller_tdata[10]} {ftop/pfconfig_i/rfdc_i/worker/ctl_rx_0_fifo_to_rx_0_marshaller_tdata[11]} {ftop/pfconfig_i/rfdc_i/worker/ctl_rx_0_fifo_to_rx_0_marshaller_tdata[12]} {ftop/pfconfig_i/rfdc_i/worker/ctl_rx_0_fifo_to_rx_0_marshaller_tdata[13]} {ftop/pfconfig_i/rfdc_i/worker/ctl_rx_0_fifo_to_rx_0_marshaller_tdata[14]} {ftop/pfconfig_i/rfdc_i/worker/ctl_rx_0_fifo_to_rx_0_marshaller_tdata[15]} {ftop/pfconfig_i/rfdc_i/worker/ctl_rx_0_fifo_to_rx_0_marshaller_tdata[16]} {ftop/pfconfig_i/rfdc_i/worker/ctl_rx_0_fifo_to_rx_0_marshaller_tdata[17]} {ftop/pfconfig_i/rfdc_i/worker/ctl_rx_0_fifo_to_rx_0_marshaller_tdata[18]} {ftop/pfconfig_i/rfdc_i/worker/ctl_rx_0_fifo_to_rx_0_marshaller_tdata[19]} {ftop/pfconfig_i/rfdc_i/worker/ctl_rx_0_fifo_to_rx_0_marshaller_tdata[20]} {ftop/pfconfig_i/rfdc_i/worker/ctl_rx_0_fifo_to_rx_0_marshaller_tdata[21]} {ftop/pfconfig_i/rfdc_i/worker/ctl_rx_0_fifo_to_rx_0_marshaller_tdata[22]} {ftop/pfconfig_i/rfdc_i/worker/ctl_rx_0_fifo_to_rx_0_marshaller_tdata[23]} {ftop/pfconfig_i/rfdc_i/worker/ctl_rx_0_fifo_to_rx_0_marshaller_tdata[24]} {ftop/pfconfig_i/rfdc_i/worker/ctl_rx_0_fifo_to_rx_0_marshaller_tdata[25]} {ftop/pfconfig_i/rfdc_i/worker/ctl_rx_0_fifo_to_rx_0_marshaller_tdata[26]} {ftop/pfconfig_i/rfdc_i/worker/ctl_rx_0_fifo_to_rx_0_marshaller_tdata[27]} {ftop/pfconfig_i/rfdc_i/worker/ctl_rx_0_fifo_to_rx_0_marshaller_tdata[28]} {ftop/pfconfig_i/rfdc_i/worker/ctl_rx_0_fifo_to_rx_0_marshaller_tdata[29]} {ftop/pfconfig_i/rfdc_i/worker/ctl_rx_0_fifo_to_rx_0_marshaller_tdata[30]} {ftop/pfconfig_i/rfdc_i/worker/ctl_rx_0_fifo_to_rx_0_marshaller_tdata[31]} ]]
#create_debug_port u_ila_0 probe
#set_property port_width 16 [get_debug_ports u_ila_0/probe7]
#set_property PROBE_TYPE DATA [get_debug_ports u_ila_0/probe7]
#connect_debug_port u_ila_0/probe7 [get_nets [list {ftop/pfconfig_i/rfdc_i/worker/ctl_rx_0_fifo_to_ctl_0_marshaller_pro[sample][data][real][0]} {ftop/pfconfig_i/rfdc_i/worker/ctl_rx_0_fifo_to_ctl_0_marshaller_pro[sample][data][real][1]} {ftop/pfconfig_i/rfdc_i/worker/ctl_rx_0_fifo_to_ctl_0_marshaller_pro[sample][data][real][2]} {ftop/pfconfig_i/rfdc_i/worker/ctl_rx_0_fifo_to_ctl_0_marshaller_pro[sample][data][real][3]} {ftop/pfconfig_i/rfdc_i/worker/ctl_rx_0_fifo_to_ctl_0_marshaller_pro[sample][data][real][4]} {ftop/pfconfig_i/rfdc_i/worker/ctl_rx_0_fifo_to_ctl_0_marshaller_pro[sample][data][real][5]} {ftop/pfconfig_i/rfdc_i/worker/ctl_rx_0_fifo_to_ctl_0_marshaller_pro[sample][data][real][6]} {ftop/pfconfig_i/rfdc_i/worker/ctl_rx_0_fifo_to_ctl_0_marshaller_pro[sample][data][real][7]} {ftop/pfconfig_i/rfdc_i/worker/ctl_rx_0_fifo_to_ctl_0_marshaller_pro[sample][data][real][8]} {ftop/pfconfig_i/rfdc_i/worker/ctl_rx_0_fifo_to_ctl_0_marshaller_pro[sample][data][real][9]} {ftop/pfconfig_i/rfdc_i/worker/ctl_rx_0_fifo_to_ctl_0_marshaller_pro[sample][data][real][10]} {ftop/pfconfig_i/rfdc_i/worker/ctl_rx_0_fifo_to_ctl_0_marshaller_pro[sample][data][real][11]} {ftop/pfconfig_i/rfdc_i/worker/ctl_rx_0_fifo_to_ctl_0_marshaller_pro[sample][data][real][12]} {ftop/pfconfig_i/rfdc_i/worker/ctl_rx_0_fifo_to_ctl_0_marshaller_pro[sample][data][real][13]} {ftop/pfconfig_i/rfdc_i/worker/ctl_rx_0_fifo_to_ctl_0_marshaller_pro[sample][data][real][14]} {ftop/pfconfig_i/rfdc_i/worker/ctl_rx_0_fifo_to_ctl_0_marshaller_pro[sample][data][real][15]} ]]
#create_debug_port u_ila_0 probe
#set_property port_width 16 [get_debug_ports u_ila_0/probe8]
#set_property PROBE_TYPE DATA [get_debug_ports u_ila_0/probe8]
#connect_debug_port u_ila_0/probe8 [get_nets [list {ftop/pfconfig_i/rfdc_i/worker/ctl_rx_0_fifo_to_ctl_0_marshaller_pro[sample][data][imaginary][0]} {ftop/pfconfig_i/rfdc_i/worker/ctl_rx_0_fifo_to_ctl_0_marshaller_pro[sample][data][imaginary][1]} {ftop/pfconfig_i/rfdc_i/worker/ctl_rx_0_fifo_to_ctl_0_marshaller_pro[sample][data][imaginary][2]} {ftop/pfconfig_i/rfdc_i/worker/ctl_rx_0_fifo_to_ctl_0_marshaller_pro[sample][data][imaginary][3]} {ftop/pfconfig_i/rfdc_i/worker/ctl_rx_0_fifo_to_ctl_0_marshaller_pro[sample][data][imaginary][4]} {ftop/pfconfig_i/rfdc_i/worker/ctl_rx_0_fifo_to_ctl_0_marshaller_pro[sample][data][imaginary][5]} {ftop/pfconfig_i/rfdc_i/worker/ctl_rx_0_fifo_to_ctl_0_marshaller_pro[sample][data][imaginary][6]} {ftop/pfconfig_i/rfdc_i/worker/ctl_rx_0_fifo_to_ctl_0_marshaller_pro[sample][data][imaginary][7]} {ftop/pfconfig_i/rfdc_i/worker/ctl_rx_0_fifo_to_ctl_0_marshaller_pro[sample][data][imaginary][8]} {ftop/pfconfig_i/rfdc_i/worker/ctl_rx_0_fifo_to_ctl_0_marshaller_pro[sample][data][imaginary][9]} {ftop/pfconfig_i/rfdc_i/worker/ctl_rx_0_fifo_to_ctl_0_marshaller_pro[sample][data][imaginary][10]} {ftop/pfconfig_i/rfdc_i/worker/ctl_rx_0_fifo_to_ctl_0_marshaller_pro[sample][data][imaginary][11]} {ftop/pfconfig_i/rfdc_i/worker/ctl_rx_0_fifo_to_ctl_0_marshaller_pro[sample][data][imaginary][12]} {ftop/pfconfig_i/rfdc_i/worker/ctl_rx_0_fifo_to_ctl_0_marshaller_pro[sample][data][imaginary][13]} {ftop/pfconfig_i/rfdc_i/worker/ctl_rx_0_fifo_to_ctl_0_marshaller_pro[sample][data][imaginary][14]} {ftop/pfconfig_i/rfdc_i/worker/ctl_rx_0_fifo_to_ctl_0_marshaller_pro[sample][data][imaginary][15]} ]]
#create_debug_port u_ila_0 probe
#set_property port_width 64 [get_debug_ports u_ila_0/probe9]
#set_property PROBE_TYPE DATA [get_debug_ports u_ila_0/probe9]
#connect_debug_port u_ila_0/probe9 [get_nets [list {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[time][fraction][0]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[time][fraction][1]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[time][fraction][2]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[time][fraction][3]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[time][fraction][4]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[time][fraction][5]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[time][fraction][6]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[time][fraction][7]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[time][fraction][8]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[time][fraction][9]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[time][fraction][10]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[time][fraction][11]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[time][fraction][12]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[time][fraction][13]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[time][fraction][14]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[time][fraction][15]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[time][fraction][16]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[time][fraction][17]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[time][fraction][18]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[time][fraction][19]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[time][fraction][20]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[time][fraction][21]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[time][fraction][22]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[time][fraction][23]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[time][fraction][24]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[time][fraction][25]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[time][fraction][26]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[time][fraction][27]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[time][fraction][28]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[time][fraction][29]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[time][fraction][30]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[time][fraction][31]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[time][fraction][32]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[time][fraction][33]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[time][fraction][34]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[time][fraction][35]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[time][fraction][36]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[time][fraction][37]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[time][fraction][38]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[time][fraction][39]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[time][fraction][40]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[time][fraction][41]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[time][fraction][42]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[time][fraction][43]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[time][fraction][44]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[time][fraction][45]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[time][fraction][46]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[time][fraction][47]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[time][fraction][48]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[time][fraction][49]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[time][fraction][50]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[time][fraction][51]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[time][fraction][52]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[time][fraction][53]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[time][fraction][54]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[time][fraction][55]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[time][fraction][56]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[time][fraction][57]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[time][fraction][58]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[time][fraction][59]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[time][fraction][60]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[time][fraction][61]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[time][fraction][62]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[time][fraction][63]} ]]
#create_debug_port u_ila_0 probe
#set_property port_width 32 [get_debug_ports u_ila_0/probe10]
#set_property PROBE_TYPE DATA [get_debug_ports u_ila_0/probe10]
#connect_debug_port u_ila_0/probe10 [get_nets [list {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[time][seconds][0]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[time][seconds][1]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[time][seconds][2]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[time][seconds][3]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[time][seconds][4]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[time][seconds][5]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[time][seconds][6]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[time][seconds][7]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[time][seconds][8]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[time][seconds][9]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[time][seconds][10]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[time][seconds][11]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[time][seconds][12]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[time][seconds][13]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[time][seconds][14]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[time][seconds][15]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[time][seconds][16]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[time][seconds][17]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[time][seconds][18]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[time][seconds][19]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[time][seconds][20]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[time][seconds][21]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[time][seconds][22]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[time][seconds][23]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[time][seconds][24]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[time][seconds][25]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[time][seconds][26]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[time][seconds][27]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[time][seconds][28]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[time][seconds][29]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[time][seconds][30]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[time][seconds][31]} ]]
#create_debug_port u_ila_0 probe
#set_property port_width 16 [get_debug_ports u_ila_0/probe11]
#set_property PROBE_TYPE DATA [get_debug_ports u_ila_0/probe11]
#connect_debug_port u_ila_0/probe11 [get_nets [list {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[sample][data][real][0]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[sample][data][real][1]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[sample][data][real][2]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[sample][data][real][3]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[sample][data][real][4]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[sample][data][real][5]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[sample][data][real][6]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[sample][data][real][7]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[sample][data][real][8]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[sample][data][real][9]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[sample][data][real][10]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[sample][data][real][11]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[sample][data][real][12]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[sample][data][real][13]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[sample][data][real][14]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[sample][data][real][15]} ]]
#create_debug_port u_ila_0 probe
#set_property port_width 16 [get_debug_ports u_ila_0/probe12]
#set_property PROBE_TYPE DATA [get_debug_ports u_ila_0/probe12]
#connect_debug_port u_ila_0/probe12 [get_nets [list {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[sample][data][imaginary][0]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[sample][data][imaginary][1]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[sample][data][imaginary][2]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[sample][data][imaginary][3]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[sample][data][imaginary][4]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[sample][data][imaginary][5]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[sample][data][imaginary][6]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[sample][data][imaginary][7]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[sample][data][imaginary][8]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[sample][data][imaginary][9]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[sample][data][imaginary][10]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[sample][data][imaginary][11]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[sample][data][imaginary][12]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[sample][data][imaginary][13]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[sample][data][imaginary][14]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[sample][data][imaginary][15]} ]]
#create_debug_port u_ila_0 probe
#set_property port_width 32 [get_debug_ports u_ila_0/probe13]
#set_property PROBE_TYPE DATA [get_debug_ports u_ila_0/probe13]
#connect_debug_port u_ila_0/probe13 [get_nets [list {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/worker/processed_data[0]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/worker/processed_data[1]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/worker/processed_data[2]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/worker/processed_data[3]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/worker/processed_data[4]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/worker/processed_data[5]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/worker/processed_data[6]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/worker/processed_data[7]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/worker/processed_data[8]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/worker/processed_data[9]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/worker/processed_data[10]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/worker/processed_data[11]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/worker/processed_data[12]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/worker/processed_data[13]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/worker/processed_data[14]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/worker/processed_data[15]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/worker/processed_data[16]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/worker/processed_data[17]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/worker/processed_data[18]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/worker/processed_data[19]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/worker/processed_data[20]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/worker/processed_data[21]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/worker/processed_data[22]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/worker/processed_data[23]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/worker/processed_data[24]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/worker/processed_data[25]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/worker/processed_data[26]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/worker/processed_data[27]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/worker/processed_data[28]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/worker/processed_data[29]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/worker/processed_data[30]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/worker/processed_data[31]} ]]
#create_debug_port u_ila_0 probe
#set_property port_width 8 [get_debug_ports u_ila_0/probe14]
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe14]
#connect_debug_port u_ila_0/probe14 [get_nets [list {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/worker/O[0]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/worker/O[1]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/worker/O[2]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/worker/O[3]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/worker/O[4]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/worker/O[5]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/worker/O[6]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/worker/O[7]} ]]
#create_debug_port u_ila_0 probe
#set_property port_width 32 [get_debug_ports u_ila_0/probe15]
#set_property PROBE_TYPE DATA [get_debug_ports u_ila_0/probe15]
#connect_debug_port u_ila_0/probe15 [get_nets [list {ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/generator_to_converter_tdata[0]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/generator_to_converter_tdata[1]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/generator_to_converter_tdata[2]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/generator_to_converter_tdata[3]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/generator_to_converter_tdata[4]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/generator_to_converter_tdata[5]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/generator_to_converter_tdata[6]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/generator_to_converter_tdata[7]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/generator_to_converter_tdata[8]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/generator_to_converter_tdata[9]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/generator_to_converter_tdata[10]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/generator_to_converter_tdata[11]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/generator_to_converter_tdata[12]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/generator_to_converter_tdata[13]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/generator_to_converter_tdata[14]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/generator_to_converter_tdata[15]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/generator_to_converter_tdata[16]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/generator_to_converter_tdata[17]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/generator_to_converter_tdata[18]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/generator_to_converter_tdata[19]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/generator_to_converter_tdata[20]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/generator_to_converter_tdata[21]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/generator_to_converter_tdata[22]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/generator_to_converter_tdata[23]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/generator_to_converter_tdata[24]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/generator_to_converter_tdata[25]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/generator_to_converter_tdata[26]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/generator_to_converter_tdata[27]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/generator_to_converter_tdata[28]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/generator_to_converter_tdata[29]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/generator_to_converter_tdata[30]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/generator_to_converter_tdata[31]} ]]
#create_debug_port u_ila_0 probe
#set_property port_width 32 [get_debug_ports u_ila_0/probe16]
#set_property PROBE_TYPE DATA [get_debug_ports u_ila_0/probe16]
#connect_debug_port u_ila_0/probe16 [get_nets [list {ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/flush_clock_count[0]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/flush_clock_count[1]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/flush_clock_count[2]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/flush_clock_count[3]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/flush_clock_count[4]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/flush_clock_count[5]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/flush_clock_count[6]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/flush_clock_count[7]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/flush_clock_count[8]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/flush_clock_count[9]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/flush_clock_count[10]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/flush_clock_count[11]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/flush_clock_count[12]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/flush_clock_count[13]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/flush_clock_count[14]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/flush_clock_count[15]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/flush_clock_count[16]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/flush_clock_count[17]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/flush_clock_count[18]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/flush_clock_count[19]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/flush_clock_count[20]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/flush_clock_count[21]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/flush_clock_count[22]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/flush_clock_count[23]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/flush_clock_count[24]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/flush_clock_count[25]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/flush_clock_count[26]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/flush_clock_count[27]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/flush_clock_count[28]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/flush_clock_count[29]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/flush_clock_count[30]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/flush_clock_count[31]} ]]
#create_debug_port u_ila_0 probe
#set_property port_width 16 [get_debug_ports u_ila_0/probe17]
#set_property PROBE_TYPE DATA [get_debug_ports u_ila_0/probe17]
#connect_debug_port u_ila_0/probe17 [get_nets [list {ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/converter_oprotocol[sample][data][imaginary][0]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/converter_oprotocol[sample][data][imaginary][1]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/converter_oprotocol[sample][data][imaginary][2]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/converter_oprotocol[sample][data][imaginary][3]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/converter_oprotocol[sample][data][imaginary][4]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/converter_oprotocol[sample][data][imaginary][5]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/converter_oprotocol[sample][data][imaginary][6]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/converter_oprotocol[sample][data][imaginary][7]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/converter_oprotocol[sample][data][imaginary][8]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/converter_oprotocol[sample][data][imaginary][9]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/converter_oprotocol[sample][data][imaginary][10]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/converter_oprotocol[sample][data][imaginary][11]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/converter_oprotocol[sample][data][imaginary][12]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/converter_oprotocol[sample][data][imaginary][13]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/converter_oprotocol[sample][data][imaginary][14]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/converter_oprotocol[sample][data][imaginary][15]} ]]
#create_debug_port u_ila_0 probe
#set_property port_width 16 [get_debug_ports u_ila_0/probe18]
#set_property PROBE_TYPE DATA [get_debug_ports u_ila_0/probe18]
#connect_debug_port u_ila_0/probe18 [get_nets [list {ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/converter_oprotocol[sample][data][real][0]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/converter_oprotocol[sample][data][real][1]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/converter_oprotocol[sample][data][real][2]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/converter_oprotocol[sample][data][real][3]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/converter_oprotocol[sample][data][real][4]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/converter_oprotocol[sample][data][real][5]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/converter_oprotocol[sample][data][real][6]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/converter_oprotocol[sample][data][real][7]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/converter_oprotocol[sample][data][real][8]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/converter_oprotocol[sample][data][real][9]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/converter_oprotocol[sample][data][real][10]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/converter_oprotocol[sample][data][real][11]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/converter_oprotocol[sample][data][real][12]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/converter_oprotocol[sample][data][real][13]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/converter_oprotocol[sample][data][real][14]} {ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/converter_oprotocol[sample][data][real][15]} ]]
#create_debug_port u_ila_0 probe
#set_property port_width 32 [get_debug_ports u_ila_0/probe19]
#set_property PROBE_TYPE DATA [get_debug_ports u_ila_0/probe19]
#connect_debug_port u_ila_0/probe19 [get_nets [list {ftop/pfconfig_i/rfdc_i/worker/ctl_0_cdc_to_ctl_rx_0_fifo_tdata[0]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_cdc_to_ctl_rx_0_fifo_tdata[1]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_cdc_to_ctl_rx_0_fifo_tdata[2]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_cdc_to_ctl_rx_0_fifo_tdata[3]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_cdc_to_ctl_rx_0_fifo_tdata[4]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_cdc_to_ctl_rx_0_fifo_tdata[5]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_cdc_to_ctl_rx_0_fifo_tdata[6]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_cdc_to_ctl_rx_0_fifo_tdata[7]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_cdc_to_ctl_rx_0_fifo_tdata[8]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_cdc_to_ctl_rx_0_fifo_tdata[9]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_cdc_to_ctl_rx_0_fifo_tdata[10]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_cdc_to_ctl_rx_0_fifo_tdata[11]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_cdc_to_ctl_rx_0_fifo_tdata[12]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_cdc_to_ctl_rx_0_fifo_tdata[13]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_cdc_to_ctl_rx_0_fifo_tdata[14]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_cdc_to_ctl_rx_0_fifo_tdata[15]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_cdc_to_ctl_rx_0_fifo_tdata[16]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_cdc_to_ctl_rx_0_fifo_tdata[17]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_cdc_to_ctl_rx_0_fifo_tdata[18]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_cdc_to_ctl_rx_0_fifo_tdata[19]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_cdc_to_ctl_rx_0_fifo_tdata[20]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_cdc_to_ctl_rx_0_fifo_tdata[21]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_cdc_to_ctl_rx_0_fifo_tdata[22]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_cdc_to_ctl_rx_0_fifo_tdata[23]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_cdc_to_ctl_rx_0_fifo_tdata[24]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_cdc_to_ctl_rx_0_fifo_tdata[25]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_cdc_to_ctl_rx_0_fifo_tdata[26]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_cdc_to_ctl_rx_0_fifo_tdata[27]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_cdc_to_ctl_rx_0_fifo_tdata[28]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_cdc_to_ctl_rx_0_fifo_tdata[29]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_cdc_to_ctl_rx_0_fifo_tdata[30]} {ftop/pfconfig_i/rfdc_i/worker/ctl_0_cdc_to_ctl_rx_0_fifo_tdata[31]} ]]
#create_debug_port u_ila_0 probe
#set_property port_width 1 [get_debug_ports u_ila_0/probe20]
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe20]
#connect_debug_port u_ila_0/probe20 [get_nets [list ftop/pfconfig_i/rfdc_i/worker/ctl_rx_0_fifo/allow_passage ]]
#create_debug_port u_ila_0 probe
#set_property port_width 1 [get_debug_ports u_ila_0/probe21]
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe21]
#connect_debug_port u_ila_0/probe21 [get_nets [list {ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/converter_oprotocol[discontinuity]} ]]
#create_debug_port u_ila_0 probe
#set_property port_width 1 [get_debug_ports u_ila_0/probe22]
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe22]
#connect_debug_port u_ila_0/probe22 [get_nets [list {ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/converter_oprotocol[flush]} ]]
#create_debug_port u_ila_0 probe
#set_property port_width 1 [get_debug_ports u_ila_0/probe23]
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe23]
#connect_debug_port u_ila_0/probe23 [get_nets [list {ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/converter_oprotocol[metadata_vld]} ]]
#create_debug_port u_ila_0 probe
#set_property port_width 1 [get_debug_ports u_ila_0/probe24]
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe24]
#connect_debug_port u_ila_0/probe24 [get_nets [list {ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/converter_oprotocol[sample_interval_vld]} ]]
#create_debug_port u_ila_0 probe
#set_property port_width 1 [get_debug_ports u_ila_0/probe25]
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe25]
#connect_debug_port u_ila_0/probe25 [get_nets [list {ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/converter_oprotocol[sample_vld]} ]]
#create_debug_port u_ila_0 probe
#set_property port_width 1 [get_debug_ports u_ila_0/probe26]
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe26]
#connect_debug_port u_ila_0/probe26 [get_nets [list {ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/converter_oprotocol[time_vld]} ]]
#create_debug_port u_ila_0 probe
#set_property port_width 1 [get_debug_ports u_ila_0/probe27]
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe27]
#connect_debug_port u_ila_0/probe27 [get_nets [list ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/converter_ordy ]]
#create_debug_port u_ila_0 probe
#set_property port_width 1 [get_debug_ports u_ila_0/probe28]
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe28]
#connect_debug_port u_ila_0/probe28 [get_nets [list ftop/pfconfig_i/rfdc_i/worker/ctl_0_cdc_to_ctl_rx_0_fifo_tready ]]
#create_debug_port u_ila_0 probe
#set_property port_width 1 [get_debug_ports u_ila_0/probe29]
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe29]
#connect_debug_port u_ila_0/probe29 [get_nets [list ftop/pfconfig_i/rfdc_i/worker/ctl_0_cdc_to_ctl_rx_0_fifo_tvalid ]]
#create_debug_port u_ila_0 probe
#set_property port_width 1 [get_debug_ports u_ila_0/probe30]
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe30]
#connect_debug_port u_ila_0/probe30 [get_nets [list {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[discontinuity]} ]]
#create_debug_port u_ila_0 probe
#set_property port_width 1 [get_debug_ports u_ila_0/probe31]
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe31]
#connect_debug_port u_ila_0/probe31 [get_nets [list {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[flush]} ]]
#create_debug_port u_ila_0 probe
#set_property port_width 1 [get_debug_ports u_ila_0/probe32]
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe32]
#connect_debug_port u_ila_0/probe32 [get_nets [list {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[metadata_vld]} ]]
#create_debug_port u_ila_0 probe
#set_property port_width 1 [get_debug_ports u_ila_0/probe33]
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe33]
#connect_debug_port u_ila_0/probe33 [get_nets [list {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[sample_interval_vld]} ]]
#create_debug_port u_ila_0 probe
#set_property port_width 1 [get_debug_ports u_ila_0/probe34]
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe34]
#connect_debug_port u_ila_0/probe34 [get_nets [list {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[sample_vld]} ]]
#create_debug_port u_ila_0 probe
#set_property port_width 1 [get_debug_ports u_ila_0/probe35]
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe35]
#connect_debug_port u_ila_0/probe35 [get_nets [list {ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc[time_vld]} ]]
#create_debug_port u_ila_0 probe
#set_property port_width 1 [get_debug_ports u_ila_0/probe36]
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe36]
#connect_debug_port u_ila_0/probe36 [get_nets [list ftop/pfconfig_i/rfdc_i/worker/ctl_0_demarshaller_to_ctl_tx_0_cdc_rdy ]]
#create_debug_port u_ila_0 probe
#set_property port_width 1 [get_debug_ports u_ila_0/probe37]
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe37]
#connect_debug_port u_ila_0/probe37 [get_nets [list {ftop/pfconfig_i/rfdc_i/worker/ctl_rx_0_fifo_to_ctl_0_marshaller_pro[sample_vld]} ]]
#create_debug_port u_ila_0 probe
#set_property port_width 1 [get_debug_ports u_ila_0/probe38]
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe38]
#connect_debug_port u_ila_0/probe38 [get_nets [list {ftop/pfconfig_i/rfdc_i/worker/ctl_rx_0_fifo_to_ctl_0_marshaller_pro[time_vld]} ]]
#create_debug_port u_ila_0 probe
#set_property port_width 1 [get_debug_ports u_ila_0/probe39]
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe39]
#connect_debug_port u_ila_0/probe39 [get_nets [list ftop/pfconfig_i/rfdc_i/worker/ctl_rx_0_fifo_to_ctl_0_marshaller_rdy ]]
#create_debug_port u_ila_0 probe
#set_property port_width 1 [get_debug_ports u_ila_0/probe40]
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe40]
#connect_debug_port u_ila_0/probe40 [get_nets [list ftop/pfconfig_i/rfdc_i/worker/ctl_rx_0_fifo_to_rx_0_marshaller_tready ]]
#create_debug_port u_ila_0 probe
#set_property port_width 1 [get_debug_ports u_ila_0/probe41]
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe41]
#connect_debug_port u_ila_0/probe41 [get_nets [list ftop/pfconfig_i/rfdc_i/worker/ctl_rx_0_fifo_to_rx_0_marshaller_tvalid ]]
#create_debug_port u_ila_0 probe
#set_property port_width 1 [get_debug_ports u_ila_0/probe42]
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe42]
#connect_debug_port u_ila_0/probe42 [get_nets [list ftop/pfconfig_i/rfdc_i/worker/ctl_tx_0_demarshaller_to_tx_0_cdc_tready ]]
#create_debug_port u_ila_0 probe
#set_property port_width 1 [get_debug_ports u_ila_0/probe43]
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe43]
#connect_debug_port u_ila_0/probe43 [get_nets [list ftop/pfconfig_i/rfdc_i/worker/ctl_tx_0_demarshaller_to_tx_0_cdc_tvalid ]]
#create_debug_port u_ila_0 probe
#set_property port_width 1 [get_debug_ports u_ila_0/probe44]
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe44]
#connect_debug_port u_ila_0/probe44 [get_nets [list ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/flush_active ]]
#create_debug_port u_ila_0 probe
#set_property port_width 1 [get_debug_ports u_ila_0/probe45]
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe45]
#connect_debug_port u_ila_0/probe45 [get_nets [list ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/flush_active_r ]]
#create_debug_port u_ila_0 probe
#set_property port_width 1 [get_debug_ports u_ila_0/probe46]
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe46]
#connect_debug_port u_ila_0/probe46 [get_nets [list ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/generator_to_converter_tready ]]
#create_debug_port u_ila_0 probe
#set_property port_width 1 [get_debug_ports u_ila_0/probe47]
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe47]
#connect_debug_port u_ila_0/probe47 [get_nets [list ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/generator_to_converter_tvalid ]]
#create_debug_port u_ila_0 probe
#set_property port_width 1 [get_debug_ports u_ila_0/probe48]
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe48]
#connect_debug_port u_ila_0/probe48 [get_nets [list ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/input_eof ]]
#create_debug_port u_ila_0 probe
#set_property port_width 1 [get_debug_ports u_ila_0/probe49]
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe49]
#connect_debug_port u_ila_0/probe49 [get_nets [list ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/input_eom ]]
#create_debug_port u_ila_0 probe
#set_property port_width 1 [get_debug_ports u_ila_0/probe50]
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe50]
#connect_debug_port u_ila_0/probe50 [get_nets [list ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/worker/input_eom ]]
#create_debug_port u_ila_0 probe
#set_property port_width 1 [get_debug_ports u_ila_0/probe51]
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe51]
#connect_debug_port u_ila_0/probe51 [get_nets [list ftop/fs_div_4_csts_tx_rx_resample_i/assy/generator0/rv/worker/marshaller_irdy ]]
#create_debug_port u_ila_0 probe
#set_property port_width 1 [get_debug_ports u_ila_0/probe52]
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe52]
#connect_debug_port u_ila_0/probe52 [get_nets [list ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/output_ready ]]
#create_debug_port u_ila_0 probe
#set_property port_width 1 [get_debug_ports u_ila_0/probe53]
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe53]
#connect_debug_port u_ila_0/probe53 [get_nets [list ftop/fs_div_4_csts_tx_rx_resample_i/assy/interpolator0/rv/worker/output_ready ]]
#create_debug_port u_ila_0 probe
#set_property port_width 1 [get_debug_ports u_ila_0/probe54]
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe54]
#connect_debug_port u_ila_0/probe54 [get_nets [list ftop/pfconfig_i/rfdc_i/worker/ctl_rx_0_fifo/p_0_in ]]
#create_debug_port u_ila_0 probe
#set_property port_width 1 [get_debug_ports u_ila_0/probe55]
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe55]
#connect_debug_port u_ila_0/probe55 [get_nets [list ftop/pfconfig_i/rfdc_i/worker/ctl_rx_0_fifo/tvalid ]]
#set_property port_width 32 [get_debug_ports u_ila_1/probe0]
#set_property PROBE_TYPE DATA [get_debug_ports u_ila_1/probe0]
#connect_debug_port u_ila_1/probe0 [get_nets [list {ftop/pfconfig_i/rfdc_i/worker/rfdc_prim_to_ctl_0_cdc_tdata[0]} {ftop/pfconfig_i/rfdc_i/worker/rfdc_prim_to_ctl_0_cdc_tdata[1]} {ftop/pfconfig_i/rfdc_i/worker/rfdc_prim_to_ctl_0_cdc_tdata[2]} {ftop/pfconfig_i/rfdc_i/worker/rfdc_prim_to_ctl_0_cdc_tdata[3]} {ftop/pfconfig_i/rfdc_i/worker/rfdc_prim_to_ctl_0_cdc_tdata[4]} {ftop/pfconfig_i/rfdc_i/worker/rfdc_prim_to_ctl_0_cdc_tdata[5]} {ftop/pfconfig_i/rfdc_i/worker/rfdc_prim_to_ctl_0_cdc_tdata[6]} {ftop/pfconfig_i/rfdc_i/worker/rfdc_prim_to_ctl_0_cdc_tdata[7]} {ftop/pfconfig_i/rfdc_i/worker/rfdc_prim_to_ctl_0_cdc_tdata[8]} {ftop/pfconfig_i/rfdc_i/worker/rfdc_prim_to_ctl_0_cdc_tdata[9]} {ftop/pfconfig_i/rfdc_i/worker/rfdc_prim_to_ctl_0_cdc_tdata[10]} {ftop/pfconfig_i/rfdc_i/worker/rfdc_prim_to_ctl_0_cdc_tdata[11]} {ftop/pfconfig_i/rfdc_i/worker/rfdc_prim_to_ctl_0_cdc_tdata[12]} {ftop/pfconfig_i/rfdc_i/worker/rfdc_prim_to_ctl_0_cdc_tdata[13]} {ftop/pfconfig_i/rfdc_i/worker/rfdc_prim_to_ctl_0_cdc_tdata[14]} {ftop/pfconfig_i/rfdc_i/worker/rfdc_prim_to_ctl_0_cdc_tdata[15]} {ftop/pfconfig_i/rfdc_i/worker/rfdc_prim_to_ctl_0_cdc_tdata[16]} {ftop/pfconfig_i/rfdc_i/worker/rfdc_prim_to_ctl_0_cdc_tdata[17]} {ftop/pfconfig_i/rfdc_i/worker/rfdc_prim_to_ctl_0_cdc_tdata[18]} {ftop/pfconfig_i/rfdc_i/worker/rfdc_prim_to_ctl_0_cdc_tdata[19]} {ftop/pfconfig_i/rfdc_i/worker/rfdc_prim_to_ctl_0_cdc_tdata[20]} {ftop/pfconfig_i/rfdc_i/worker/rfdc_prim_to_ctl_0_cdc_tdata[21]} {ftop/pfconfig_i/rfdc_i/worker/rfdc_prim_to_ctl_0_cdc_tdata[22]} {ftop/pfconfig_i/rfdc_i/worker/rfdc_prim_to_ctl_0_cdc_tdata[23]} {ftop/pfconfig_i/rfdc_i/worker/rfdc_prim_to_ctl_0_cdc_tdata[24]} {ftop/pfconfig_i/rfdc_i/worker/rfdc_prim_to_ctl_0_cdc_tdata[25]} {ftop/pfconfig_i/rfdc_i/worker/rfdc_prim_to_ctl_0_cdc_tdata[26]} {ftop/pfconfig_i/rfdc_i/worker/rfdc_prim_to_ctl_0_cdc_tdata[27]} {ftop/pfconfig_i/rfdc_i/worker/rfdc_prim_to_ctl_0_cdc_tdata[28]} {ftop/pfconfig_i/rfdc_i/worker/rfdc_prim_to_ctl_0_cdc_tdata[29]} {ftop/pfconfig_i/rfdc_i/worker/rfdc_prim_to_ctl_0_cdc_tdata[30]} {ftop/pfconfig_i/rfdc_i/worker/rfdc_prim_to_ctl_0_cdc_tdata[31]} ]]
#create_debug_port u_ila_1 probe
#set_property port_width 16 [get_debug_ports u_ila_1/probe1]
#set_property PROBE_TYPE DATA [get_debug_ports u_ila_1/probe1]
#connect_debug_port u_ila_1/probe1 [get_nets [list {ftop/pfconfig_i/rfdc_i/worker/rfdc_prim/rfdc_to_rx_0_combiner_1_tdata[0]} {ftop/pfconfig_i/rfdc_i/worker/rfdc_prim/rfdc_to_rx_0_combiner_1_tdata[1]} {ftop/pfconfig_i/rfdc_i/worker/rfdc_prim/rfdc_to_rx_0_combiner_1_tdata[2]} {ftop/pfconfig_i/rfdc_i/worker/rfdc_prim/rfdc_to_rx_0_combiner_1_tdata[3]} {ftop/pfconfig_i/rfdc_i/worker/rfdc_prim/rfdc_to_rx_0_combiner_1_tdata[4]} {ftop/pfconfig_i/rfdc_i/worker/rfdc_prim/rfdc_to_rx_0_combiner_1_tdata[5]} {ftop/pfconfig_i/rfdc_i/worker/rfdc_prim/rfdc_to_rx_0_combiner_1_tdata[6]} {ftop/pfconfig_i/rfdc_i/worker/rfdc_prim/rfdc_to_rx_0_combiner_1_tdata[7]} {ftop/pfconfig_i/rfdc_i/worker/rfdc_prim/rfdc_to_rx_0_combiner_1_tdata[8]} {ftop/pfconfig_i/rfdc_i/worker/rfdc_prim/rfdc_to_rx_0_combiner_1_tdata[9]} {ftop/pfconfig_i/rfdc_i/worker/rfdc_prim/rfdc_to_rx_0_combiner_1_tdata[10]} {ftop/pfconfig_i/rfdc_i/worker/rfdc_prim/rfdc_to_rx_0_combiner_1_tdata[11]} {ftop/pfconfig_i/rfdc_i/worker/rfdc_prim/rfdc_to_rx_0_combiner_1_tdata[12]} {ftop/pfconfig_i/rfdc_i/worker/rfdc_prim/rfdc_to_rx_0_combiner_1_tdata[13]} {ftop/pfconfig_i/rfdc_i/worker/rfdc_prim/rfdc_to_rx_0_combiner_1_tdata[14]} {ftop/pfconfig_i/rfdc_i/worker/rfdc_prim/rfdc_to_rx_0_combiner_1_tdata[15]} ]]
#create_debug_port u_ila_1 probe
#set_property port_width 16 [get_debug_ports u_ila_1/probe2]
#set_property PROBE_TYPE DATA [get_debug_ports u_ila_1/probe2]
#connect_debug_port u_ila_1/probe2 [get_nets [list {ftop/pfconfig_i/rfdc_i/worker/rfdc_prim/rfdc_to_rx_0_combiner_0_tdata[0]} {ftop/pfconfig_i/rfdc_i/worker/rfdc_prim/rfdc_to_rx_0_combiner_0_tdata[1]} {ftop/pfconfig_i/rfdc_i/worker/rfdc_prim/rfdc_to_rx_0_combiner_0_tdata[2]} {ftop/pfconfig_i/rfdc_i/worker/rfdc_prim/rfdc_to_rx_0_combiner_0_tdata[3]} {ftop/pfconfig_i/rfdc_i/worker/rfdc_prim/rfdc_to_rx_0_combiner_0_tdata[4]} {ftop/pfconfig_i/rfdc_i/worker/rfdc_prim/rfdc_to_rx_0_combiner_0_tdata[5]} {ftop/pfconfig_i/rfdc_i/worker/rfdc_prim/rfdc_to_rx_0_combiner_0_tdata[6]} {ftop/pfconfig_i/rfdc_i/worker/rfdc_prim/rfdc_to_rx_0_combiner_0_tdata[7]} {ftop/pfconfig_i/rfdc_i/worker/rfdc_prim/rfdc_to_rx_0_combiner_0_tdata[8]} {ftop/pfconfig_i/rfdc_i/worker/rfdc_prim/rfdc_to_rx_0_combiner_0_tdata[9]} {ftop/pfconfig_i/rfdc_i/worker/rfdc_prim/rfdc_to_rx_0_combiner_0_tdata[10]} {ftop/pfconfig_i/rfdc_i/worker/rfdc_prim/rfdc_to_rx_0_combiner_0_tdata[11]} {ftop/pfconfig_i/rfdc_i/worker/rfdc_prim/rfdc_to_rx_0_combiner_0_tdata[12]} {ftop/pfconfig_i/rfdc_i/worker/rfdc_prim/rfdc_to_rx_0_combiner_0_tdata[13]} {ftop/pfconfig_i/rfdc_i/worker/rfdc_prim/rfdc_to_rx_0_combiner_0_tdata[14]} {ftop/pfconfig_i/rfdc_i/worker/rfdc_prim/rfdc_to_rx_0_combiner_0_tdata[15]} ]]
#create_debug_port u_ila_1 probe
#set_property port_width 32 [get_debug_ports u_ila_1/probe3]
#set_property PROBE_TYPE DATA [get_debug_ports u_ila_1/probe3]
#connect_debug_port u_ila_1/probe3 [get_nets [list {ftop/pfconfig_i/rfdc_i/worker/tx_0_cdc_to_rfdc_prim_tdata[0]} {ftop/pfconfig_i/rfdc_i/worker/tx_0_cdc_to_rfdc_prim_tdata[1]} {ftop/pfconfig_i/rfdc_i/worker/tx_0_cdc_to_rfdc_prim_tdata[2]} {ftop/pfconfig_i/rfdc_i/worker/tx_0_cdc_to_rfdc_prim_tdata[3]} {ftop/pfconfig_i/rfdc_i/worker/tx_0_cdc_to_rfdc_prim_tdata[4]} {ftop/pfconfig_i/rfdc_i/worker/tx_0_cdc_to_rfdc_prim_tdata[5]} {ftop/pfconfig_i/rfdc_i/worker/tx_0_cdc_to_rfdc_prim_tdata[6]} {ftop/pfconfig_i/rfdc_i/worker/tx_0_cdc_to_rfdc_prim_tdata[7]} {ftop/pfconfig_i/rfdc_i/worker/tx_0_cdc_to_rfdc_prim_tdata[8]} {ftop/pfconfig_i/rfdc_i/worker/tx_0_cdc_to_rfdc_prim_tdata[9]} {ftop/pfconfig_i/rfdc_i/worker/tx_0_cdc_to_rfdc_prim_tdata[10]} {ftop/pfconfig_i/rfdc_i/worker/tx_0_cdc_to_rfdc_prim_tdata[11]} {ftop/pfconfig_i/rfdc_i/worker/tx_0_cdc_to_rfdc_prim_tdata[12]} {ftop/pfconfig_i/rfdc_i/worker/tx_0_cdc_to_rfdc_prim_tdata[13]} {ftop/pfconfig_i/rfdc_i/worker/tx_0_cdc_to_rfdc_prim_tdata[14]} {ftop/pfconfig_i/rfdc_i/worker/tx_0_cdc_to_rfdc_prim_tdata[15]} {ftop/pfconfig_i/rfdc_i/worker/tx_0_cdc_to_rfdc_prim_tdata[16]} {ftop/pfconfig_i/rfdc_i/worker/tx_0_cdc_to_rfdc_prim_tdata[17]} {ftop/pfconfig_i/rfdc_i/worker/tx_0_cdc_to_rfdc_prim_tdata[18]} {ftop/pfconfig_i/rfdc_i/worker/tx_0_cdc_to_rfdc_prim_tdata[19]} {ftop/pfconfig_i/rfdc_i/worker/tx_0_cdc_to_rfdc_prim_tdata[20]} {ftop/pfconfig_i/rfdc_i/worker/tx_0_cdc_to_rfdc_prim_tdata[21]} {ftop/pfconfig_i/rfdc_i/worker/tx_0_cdc_to_rfdc_prim_tdata[22]} {ftop/pfconfig_i/rfdc_i/worker/tx_0_cdc_to_rfdc_prim_tdata[23]} {ftop/pfconfig_i/rfdc_i/worker/tx_0_cdc_to_rfdc_prim_tdata[24]} {ftop/pfconfig_i/rfdc_i/worker/tx_0_cdc_to_rfdc_prim_tdata[25]} {ftop/pfconfig_i/rfdc_i/worker/tx_0_cdc_to_rfdc_prim_tdata[26]} {ftop/pfconfig_i/rfdc_i/worker/tx_0_cdc_to_rfdc_prim_tdata[27]} {ftop/pfconfig_i/rfdc_i/worker/tx_0_cdc_to_rfdc_prim_tdata[28]} {ftop/pfconfig_i/rfdc_i/worker/tx_0_cdc_to_rfdc_prim_tdata[29]} {ftop/pfconfig_i/rfdc_i/worker/tx_0_cdc_to_rfdc_prim_tdata[30]} {ftop/pfconfig_i/rfdc_i/worker/tx_0_cdc_to_rfdc_prim_tdata[31]} ]]
#create_debug_port u_ila_1 probe
#set_property port_width 1 [get_debug_ports u_ila_1/probe4]
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_1/probe4]
#connect_debug_port u_ila_1/probe4 [get_nets [list ftop/pfconfig_i/rfdc_i/worker/rfdc_prim_to_ctl_0_cdc_tready ]]
#create_debug_port u_ila_1 probe
#set_property port_width 1 [get_debug_ports u_ila_1/probe5]
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_1/probe5]
#connect_debug_port u_ila_1/probe5 [get_nets [list ftop/pfconfig_i/rfdc_i/worker/rfdc_prim_to_ctl_0_cdc_tvalid ]]
#create_debug_port u_ila_1 probe
#set_property port_width 1 [get_debug_ports u_ila_1/probe6]
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_1/probe6]
#connect_debug_port u_ila_1/probe6 [get_nets [list ftop/pfconfig_i/rfdc_i/worker/rfdc_prim/rfdc_to_rx_0_combiner_0_tready ]]
#create_debug_port u_ila_1 probe
#set_property port_width 1 [get_debug_ports u_ila_1/probe7]
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_1/probe7]
#connect_debug_port u_ila_1/probe7 [get_nets [list ftop/pfconfig_i/rfdc_i/worker/rfdc_prim/rfdc_to_rx_0_combiner_0_tvalid ]]
#create_debug_port u_ila_1 probe
#set_property port_width 1 [get_debug_ports u_ila_1/probe8]
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_1/probe8]
#connect_debug_port u_ila_1/probe8 [get_nets [list ftop/pfconfig_i/rfdc_i/worker/rfdc_prim/rfdc_to_rx_0_combiner_1_tready ]]
#create_debug_port u_ila_1 probe
#set_property port_width 1 [get_debug_ports u_ila_1/probe9]
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_1/probe9]
#connect_debug_port u_ila_1/probe9 [get_nets [list ftop/pfconfig_i/rfdc_i/worker/rfdc_prim/rfdc_to_rx_0_combiner_1_tvalid ]]
#create_debug_port u_ila_1 probe
#set_property port_width 1 [get_debug_ports u_ila_1/probe10]
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_1/probe10]
#connect_debug_port u_ila_1/probe10 [get_nets [list ftop/pfconfig_i/rfdc_i/worker/tx_0_cdc_to_rfdc_prim_tready ]]
#create_debug_port u_ila_1 probe
#set_property port_width 1 [get_debug_ports u_ila_1/probe11]
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_1/probe11]
#connect_debug_port u_ila_1/probe11 [get_nets [list ftop/pfconfig_i/rfdc_i/worker/tx_0_cdc_to_rfdc_prim_tvalid ]]
