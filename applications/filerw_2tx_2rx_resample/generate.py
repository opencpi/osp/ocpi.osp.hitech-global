#!/usr/bin/env python3
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public License along
# with this program. If not, see <http://www.gnu.org/licenses/>.

from argparse import ArgumentParser
import sys
import os.path
import numpy as np
import array
import opencpi.unit_test_utils as utu
import opencpi.complexshorttimedsample_utils as iqm

def main():
    if len(sys.argv) != 6:
        print("Invalid arguments:  usage is: generate.py <sample-freq> <target-freq> <amplitude> <num-samples> <output-file>")
        # Example: python3 generate.py 90000 22500 32767 8 test_msgs.bin
        sys.exit(1)
    print("    GENERATE (I/Q 16b binary data file):")

    output_file = sys.argv[5]
    
    # Create cosine & sine waveforms
    Fs = float(sys.argv[1]) # sample frequency
    Ts = 1.0/float(Fs)      # sampling interval
    Ft = float(sys.argv[2]) # target frequency
    AMPLITUDE = int(sys.argv[3])
    num_samples_to_generate = int(sys.argv[4]) # number of complex samples
    t = np.arange(0,num_samples_to_generate*Ts,Ts,dtype=np.float)   # time vector
    # Generate I/Q samples @ target freq
    real = np.cos(2*np.pi*Ft*t)
    imag = np.sin(2*np.pi*Ft*t)
    # Initialize empty array, sized to store 16b I/Q samples
    out_data = np.array(np.zeros(num_samples_to_generate), 
                        dtype=np.dtype((np.uint32, {'real_idx':(np.int16,0), 'imag_idx':(np.int16,2)})))
    # Set the gain
    gain_i = AMPLITUDE / max(abs(real))
    gain_q = AMPLITUDE / max(abs(imag))
    out_data['real_idx'] = np.int16(real * gain_i)
    out_data['imag_idx'] = np.int16(imag * gain_q)

    # Write to file
    #with open(args.output_file, 'wb') as f:
    with open(output_file, 'wb') as f:
        iqm.add_samples(f, out_data, 1, int(num_samples_to_generate))
        utu.add_msg(f, iqm.TIME_OPCODE, array.array('I',(int('00002000',16), int('00003000',16), int('00001000',16))))
        utu.add_msg(f, iqm.SAMPLE_INTERVAL_OPCODE, array.array('I',(int('00005000',16), int('00006000',16), int('00004000',16))))
        utu.add_msg(f, iqm.METADATA_OPCODE, array.array('I',(int('85555551',16), int('DEADBEEF',16), int('8AAAAAA1',16))))
        utu.add_msg(f, iqm.FLUSH_OPCODE, [])
        utu.add_msg(f, iqm.DISCONTINUITY_OPCODE, [])

        # Three 32b words: two 32b words for 64b [31:0],[63:32] fractional and a 32b seconds
        utu.add_msg(f, iqm.TIME_OPCODE, array.array('I',(int('00002000',16), int('00003000',16), int('00001000',16))))

        iqm.add_samples(f, out_data, 1, int(num_samples_to_generate))

        # Three 32b words: two 32b words for 64b [31:0],[63:32] fractional and a 32b seconds
        utu.add_msg(f, iqm.SAMPLE_INTERVAL_OPCODE, array.array('I',(int('00005000',16), int('00006000',16), int('00004000',16))))

        iqm.add_samples(f, out_data, 1, int(num_samples_to_generate))

        # Three 32b words:  one 32b metadata ID and two 32b words for 64b [31:0],[63:32] metadata value
        utu.add_msg(f, iqm.METADATA_OPCODE, array.array('I',(int('00007000',16), int('00008000',16), int('00009000',16))))

        # Three 32b words:  one 32b metadata ID and two 32b words for 64b [31:0],[63:32] metadata value
        utu.add_msg(f, iqm.METADATA_OPCODE, array.array('I',(int('85555551',16), int('DEADBEEF',16), int('8AAAAAA1',16))))

        iqm.add_samples(f, out_data, 1, int(num_samples_to_generate))

        # Three 32b words:  one 32b metadata ID and two 32b words for 64b [31:0],[63:32] metadata value
        utu.add_msg(f, iqm.METADATA_OPCODE, array.array('I',(int('00007000',16), int('00008000',16), int('00009000',16))))

        iqm.add_samples(f, out_data, 1, int(num_samples_to_generate))

        # Three 32b words:  one 32b metadata ID and two 32b words for 64b [31:0],[63:32] metadata value
        utu.add_msg(f, iqm.METADATA_OPCODE, array.array('I',(int('85555551',16), int('DEADBEEF',16), int('8AAAAAA1',16))))
        utu.add_msg(f, iqm.METADATA_OPCODE, array.array('I',(int('00007000',16), int('00008000',16), int('00009000',16))))

        iqm.add_samples(f, out_data, 1, int(num_samples_to_generate))

        utu.add_msg(f, iqm.TIME_OPCODE, array.array('I',(int('00002000',16), int('00003000',16), int('00001000',16))))

        iqm.add_samples(f, out_data, 1, int(num_samples_to_generate))

        utu.add_msg(f, iqm.FLUSH_OPCODE, [])

        utu.add_msg(f, iqm.SAMPLE_INTERVAL_OPCODE, array.array('I',(int('00005000',16), int('00006000',16), int('00004000',16))))
        utu.add_msg(f, iqm.SAMPLE_INTERVAL_OPCODE, array.array('I',(int('10005001',16), int('10006001',16), int('10004001',16))))

        utu.add_msg(f, iqm.TIME_OPCODE, array.array('I',(int('00002000',16), int('00003000',16), int('00001000',16))))

        iqm.add_samples(f, out_data, 1, int(num_samples_to_generate))

        utu.add_msg(f, iqm.DISCONTINUITY_OPCODE, [])

        utu.add_msg(f, iqm.TIME_OPCODE, array.array('I',(int('00002000',16), int('00003000',16), int('00001000',16))))
        utu.add_msg(f, iqm.TIME_OPCODE, array.array('I',(int('10002001',16), int('10003001',16), int('10001001',16))))

        utu.add_msg(f, iqm.DISCONTINUITY_OPCODE, [])
        utu.add_msg(f, iqm.FLUSH_OPCODE, [])

if __name__ == "__main__":
    main()
