set -e
ocpirun -t 20 test_j13_tuning_freq_MHz.xml
ocpirun -t 20 test_j13_gain_dB.xml
ocpirun -t 20 test_j18_tuning_freq_MHz.xml
ocpirun -t 20 test_j18_gain_dB.xml
ocpirun -t 20 test_j20_tuning_freq_MHz.xml
ocpirun -t 20 test_j20_gain_dB.xml
ocpirun -t 20 test_j3_tuning_freq_MHz.xml
ocpirun -t 20 test_j3_gain_dB.xml
ocpirun -t 20 test_j3_j13.xml
ocpirun -t 20 test_j18_j20.xml
ocpirun -t 20 test_j3_j13_j18_j20.xml
ocpirun -t 20 test_deferred.xml
ocpirun -t 20 test_multiconfig_same_rf_port.xml
ocpirun -t 20 test_multiconfig_separate_rf_ports.xml
